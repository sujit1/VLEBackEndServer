var express    = require('express'),
    app        = express(),
    mongodb = require('./db'),
    bodyParser = require('body-parser'),
    port = process.env.PORT ||  8080,
     _ =require('underscore'),      
 Buffer = require('buffer/').Buffer,
//    port = process.env.PORT ||  3000,
    Globals = require('./globals'),
    log4js = require('log4js');

var multer=require("multer");
var upload = multer({ dest: './tmp/'});
var async = require('async');
var Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = mongodb.GridStore,
    Code = require('mongodb').Code,
    BSON = require('mongodb-core').BSON,
    assert = require('assert');
    fs = require('fs'),
    Grid = require('gridfs-stream');



var formidable = require('formidable');


log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');
var fs = require('fs');

app.use(function(req,res,next){
  
  res.header('Access-Control-Allow-Origin', '*'); // We can access from anywhere
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});





app.use(express.static(__dirname + '/public/'));
app.use(express.static(__dirname + '/images/'));
app.use(bodyParser.urlencoded({   uploadDir: app.settings.tmpFolder,extended: true ,limit: '1024mb'}));
app.use(bodyParser.json({limit: '1024mb'}));
app.use(logResponseBody);




function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end,
        chunks = [],
        t1 = new Date();  

    res.write = function (chunk) {
        chunks.push(chunk);
        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk) {
        if (chunk)
            chunks.push(chunk);
        var t2 = new Date();
        
        oldEnd.apply(res, arguments);
    };

    next();
};


process.on('SIGINT', function() {
    mongodb.close(function(){
        logger.info('closing db');
        process.exit(0);
    });
});

process.on('uncaughtException', function(err) {
    // handle the error safely
    logger.info(err.stack);
});



mongodb.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){

if (err) {

  logger.info("Problem in connecting to MongoDB."+err);
}
else
{
  logger.info("Connected to MongoDB");
  app.listen(port,function(){
    logger.info('API\'s work at http://localhost:' +port+ "url.");
  });
}


});

app.get('/', function(req, res) {
    res.sendFile(Globals.appRoot + '/public/form.html');
});



var organizationController = require('./Controllers/OrganizationController');
var projectController = require('./Controllers/ProjectController');
var installationController = require('./Controllers/InstallationController');



////////////////////////////////// Creating Categories //////////////////////////////////
app.post('/addingCategories',function(req,res){

var categories=
[
    {
        categoryName:"SOLAR STREET LIGHT",
    },
    {
        categoryName:"SOLAR HOME LIGHT",
    },
    {
        categoryName:"SOLAR INVERTER SYSTEM",
    },
    {
        categoryName:"SOLAR LANTERN",
    }
]

    mongodb.save('Categories',categories,function(err,result){
        if(err){
            res.json(err);
        }
        else
            {
                res.json(result);
            }

    })

})

app.get('/getAllCategories',function(req,res){
    mongodb.findAll('Categories',function(err,result){
        if(err) res.json(err);
            else
                {
                    if(result.length>0)
                        {
                    res.json(result);
                        }
                        else
                            {
                                res.json({"status":1,"message":"No categories found"});           
                            }
                }
    })
});



////////////////////////////////// Organization Registeration //////////////////////////////////


app.post('/newOrganizationRegisteration',function(req,res){
organizationController.addNewOrganization(req.body,function(err,details){
if(err) 
    res.json({"status":1,"message":"error in insertion of the record"});
    else
        {
            if(details.result){
            delete details.result.password
            }
            res.json(details);
        }

});

});


app.post('/OTPConfirmation/:_id',function(req,res){

    organizationController.OTPConfirmation(req.params,function(err,result){
        if(err){
            res.json({"status":1,"message":"error in updating the record"});
        }
        else
        {
            if(result.result){
                if(result.result.n>0)
                {
                    res.json({"status":0,"message":"Successfully authenticated"});
                }
                else
                {
                    res.json({"status":1,"message":"Unable to update the record"});
                }
            }
            else
            {
                res.json(result);
            }
        }

    })
});

////////////////////////////////// Adding Existing Installer to his organization //////////////////////////////////
app.post('/addCurrentInstallerToThereOrganization',function(req,res){
 organizationController.addCurrentInstallerToThereOrganization(req.body,function(err,details){
    if(err) 
        res.json({"status":1,"message":"error in insertion of the record"});
        else
            {
                if(details.result){
                delete details.result.password
                }
                res.json(details);
            }
    
    });

});


////////////////////////////////// Organization Login //////////////////////////////////

app.post('/organizationLogin',function(req,res){

    //new user(null,null,req.body.emailID,req.body.password,null);

    organizationController.login(req.body,function(err,loginDetails){
if(err) res.json({"status":1,"message":"error in login"});
else
    {
    if(loginDetails!= null)
     {

        if(loginDetails.result){   
        delete loginDetails.result.password
        }
        res.json(loginDetails);
        
     }
    else
        {
            res.json({"status":1,"message":"Invalid EmailID or Password please check your details"});            
        }
    }
    });
  

});


app.post('/deleteInstaller',function(req,res){
    organizationController.deleteInstaller(req.body,function(err,result){
        if(err) res.json({"status":1,"message":err});
        else
            {
                if(result.result)
                    {
                    
                if(result.result.n>0){
                    res.json({"status":0,"message":"Successfully deleted"});
                }
                else
                    {
                        res.json({"status":1,"message":"Unable to delete the record"});
                    }
                }
                else
                    {
                        res.json(result);
                    }


            }
    });
});










//var installerController = require('./Controllers/InstallerController');


////////////////////////////////// Add Installers //////////////////////////////////


// app.post('/addInstaller',function(req,res){

//     var newInstaller=new installer(req.body.organization_Id,req.body.installer_first_name,req.body.installer_last_name,req.body.installer_emailId,req.body.password);

//     installerController.addNewInstaller(newInstaller,function(err,result){

//         if(err){
//             res.json(err);
//         }
//         else
//             {
//                 res.json(result);
//             }

//     });


// });




// app.post('/installerLogin',function(req,res){
    
//         var loginInstallerUser={
//             installer_emailId:req.body.installer_emailId,
//             password:req.body.password
//         }; //new user(null,null,req.body.emailID,req.body.password,null);
    
//         installerController.login(loginInstallerUser,function(err,loginDetails){
//     if(err) res.json({"status":0,"message":"error in with login"});
//     else
//         {
//         if(loginDetails!= null)
//          {
//             res.json(loginDetails);
            
//          }
//         else
//             {
//                 res.json({"status":0,"message":"Invalid EmailID or Password Please check your details"});            
//             }
//         }
//         });
      
    
//     });
    





////////////////////////////////// Forgot Password //////////////////////////////////

app.post('/changePassword',function(req,res){

    organizationController.changePassword(req.body,function(err,results){
if(err)
    {
        res.json(err);
    }
    else
        {
            if(results.result){
                 if(results.result.result.n>0)
                    {
                      //  res.json({"status":0,"message":"Successfully changed your password"});            
                                                res.json({"status":0,"message":"Successfully changed your password","result":results.useDetails});            

                    }
                    else
                        {
                            res.json({"status":1,"message":"Unable to change your password"});            
                        }
            }
            else
                {
                    res.json(results);
                }
          
        }
    });


});


////////////////////////////////// forgot OTP //////////////////////////////////

app.post('/forgotPassword',function(req,res){
    
        organizationController.forgotPassword(req.body,function(err,results){
    if(err)
        {
            res.json(err);
        }
        else
            {
                if(results.result){
                    //if(results.result.n>0)
		if(results.result.result.n>0)
                        {
                          //  res.json({"status":0,"message":"Successfully sent OTP to your email ID"});            
                         res.json({"status":0,"message":"Successfully sent OTP to your email ID","OTP":results.OTP,"isOrganization":results.isOrganization,"_id":results._id});            

                        }
                        else
                            {
                                res.json({"status":1,"message":"Unable to send OTP to your EmailID"});            
                            }
                }
                else
                    {
                        res.json(results);
                    }
              
            }
        });
    
    
    });
    
    // app.post('/resendOTP',function(req,res){

    // })


    //////////////////////////// getCompleteDetailsOfThisOrganization/////////////////////////

    
    app.get('/getCompleteDetailsOfThisOrganization/:_id',function(req,res){

        organizationController.getCompleteDetailsOfThisOrganization(req.params,function(err,result){
            if(err){
                res.json(err);
            }               
            else
            {
                res.json(result);
            }
        })

    })
////////////////////////////////// Add Project //////////////////////////////////

app.post('/addProject',function(req,res){
    projectController.addNewProject(req.body,function(error,result){
        if(error){
            res.json(error);
        }
        else
            {
		console.log("Project details are " +result);
                logger.info("Project details are " +result);
                res.json(result);
                
            }
    });

});


app.post('/updateProject',function(req,res){
    
        projectController.updateProject(req.body,function(error,result){
            if(error){
                res.json(error);
            }
            else
                {
                    
                    res.json(result);
                  
                    
                }
        });
    
    });



app.post('/addInstallersToProject',function(req,res){

    projectController.addInstallersToProject(req.body,function(err,result){
        if(err)
        {
            res.json({"status":1,"message":"Error in adding installers to project"});
        }
        else
        {
            res.json(result)
        }
    })
})

// For organization to get all projects of an organization

app.get('/allProjectsOfAnOrganization/:organization_Id',function(req,res){
    projectController.allProjectsOfAnOrganization(req.params,function(error,result){
        if(error){
            res.json(error);
        }
        else
            {
                res.json(result);
            }
    });

});

///////////////////////////////////////////////////////Delete projects//////////////////////////////////////////////////////////////////

app.post('/deleteProject',function(req,res){
    projectController.deleteProject(req.body,function(err,result){
        if(err) res.json({"status":1,"message":"Error in deletetion of record"});
        else
            {
                if(result.result)
                    {
                    
                if(result.result.n>0){
                    res.json({"status":0,"message":"Successfully deleted"});
                }
                else
                    {
                        res.json({"status":1,"message":"Unable to delete the record"});
                    }
                }
                else
                    {
                        res.json(result);
                    }


            }
    });
});
app.post('/deleteInstallerFromProject',function(req,res){
    projectController.deleteInstallerFromProject(req.body,function(err,result){
        if(err) res.json({"status":1,"message":"Error in deletetion of record"});
        else
            {
                if(result.result)
                    {
                    
                if(result.result.n>0){
                    res.json({"status":0,"message":"Successfully deleted"});
                }
                else
                    {
                        res.json({"status":1,"message":"Unable to delete the record"});
                    }
                }
                else
                    {
                        res.json(result);
                    }


            }
    });
});


//For organization get all installers inside a project 

app.get('/allInstallersInProject/:_id',function(req,res){
    projectController.getAllInstallersInProject(req.params,function(error,result){
        if(error){
            res.json(error);
        }
        else
            {
                res.json(result);
            }
    });

});

//For installer to get all projects which he is involved

app.get('/getAllProjectsInstallerInvolvedIn/:installer_id',function(req,res){
    projectController.getAllProjectsInstallerInvolvedIn(req.params,function(error,result){
        if(error){
            res.json(error);
        }
        else
            {
                res.json(result);
            }
    });

});
app.get('/getAllOrganizationsOfInstallerInvolvedIn/:installer_id', function(req, res) {
            organizationController.getAllOrganizationsOfInstallerInvolvedIn(req.params,function(err,result){
                if(err) res.json(err);
                else
                    {
                        res.json(result);
                    }
            });
});


//////////////////////////////////show all device details//////////////////////////////////

app.get('/allInstallations',function(req,res){
    installationController.getAllInstallations(function(){
        if(err)
            {
                res.json(err);
            }
            else
                {
                    res.json(result);
                }
    });
});


   ////////////////////////////////// Add installations//////////////////////////////


app.post('/addInstallations',upload.any(),function(req,res){

        installationController.addNewInstallation(req.body,function(err,result){
            if(err)
                {
                    res.json(err);
                }
                else
                    {
                        res.json(result);
                    }
        });
});

app.get('/viewAllInstallations/:assignment_id',upload.any(),function(req,res){
    installationController.viewAllInstallations(req.params,function(err,result){
        if(err){
            res.json(err);
        }
        else
        {
            res.json(result);
        }
    })
})



app.post('/updateInstallation',upload.any(),function(req,res){
    installationController.updateInstallation(req.body,function(err,result){
        if(err){
            res.json(err);
        }
        else
        {
            res.json(result);
        }
    })
})



app.post('/deleteInstallations/:_id',upload.any(),function(req,res){
    installationController.deleteInstallations(req.params,function(err,result){
        if(err){
            res.json(err);
        }
        else
        {
            res.json(result);
        }
    })
})

///////////////////////// Update device details ///////////////////////////

// app.post('/updateUser/:deviceID',upload.any(),function(req,res){

    
//     var updateFrom = { deviceID: req.params.deviceID};
//     var updateTo = {$set: {deviceName:req.body.deviceName,deviceSNO:req.body.deviceSNO, deviceType:req.body.deviceType}};
    
//     mongodb.updateByObjects("userDetails",updateFrom,updateTo,function(err,deviceResult){
        
//         if(err)
//                 {
//                     res.send({"status":0,"message":"error in retriving the record"});
                    
//                 }
//                 else
//                     {
//         if(deviceResult)
//             {
//                 res.send({"status":0,"message":"Updated Successfully"});
//             }
//             else
//                 {
//                     res.send({"status":0,"message":"Device ID not found"});
                    
//                 }
//             }

//   });

// });


////////////////////////// Get device details//////////////////////////

// app.get('/organization/:installationId',function(req,res){

//     if (req.params.installationId)
//         {
//             var query =
//             {
//             "generalInformations.installationId":req.params.installationId
//             };

//  mongodb.findByObjects("userDetails",query,function(err,results){
    
//         if(err)
//             {
//                 res.send({"status":0,"message":"error in retriving the record"});
                
//             }
//             else
//                 { 
//                     if(results.length>0)
//                      {
//                         res.json(results);
                        
//                         }
//                      else
//                          {
//                             res.send({"status":0,"message":"No devices found with this ID"});
                            
//                           }
//             }


//      });

//         }
//         else
//             {
//                 res.send({"status":0,"message":"Please send us the ID which you want to retrive from."});
                
//             }

// });

// //////////////////show and display DeviceImage//////////////////////////////



// app.get('/image/:_id',function(req,res){


// logger.info('id information'+req.params._id);
// if (req.params._id)
//  {
//  var mongodbs = require('mongodb');
//  var pi_id   = new ObjectID(req.params._id);

// mongodb.dbDetails.collection('usersImage.files')
//   .find({_id: pi_id})
//   .toArray(function(err, files) {
//     if (err) throw err;
//     files.forEach(function(file) {
//         if (file) {    
//  var gfs = Grid(mongodb,mongodbs);

//    var bucket = new mongodbs.GridFSBucket(mongodb.dbDetails, {
//             chunkSizeBytes: 1024,
//             bucketName: 'usersImage'
//         });

//    var readStream = bucket.openDownloadStream(pi_id); // The readable stream

// res.once("finish", function (data) {

// res.end(data);
// });

// res.on("error", function (err) {
// next(err);
// });


// var path = require('path')
// var extname = path.extname(file.filename).substring(1);
// console.log('image/'+extname);
// res.header("content-type", 'image/'+extname);
// readStream.pipe(res);


//   }
//   else{
//     console.log('asdfasdfasdfsdf');
//   }
//     });
//   });


//   }
//   else
//   {
//      res.send({"status":0,"message":"Please give the image ID"});
//   }

// });


////////////////////////Delete and device from mongo//////////////////////////////

// app.delete('/deletedevice/:id',function(req,res){
//     var id=req.params._id;
  
//     mongodb.deleteManyByObject("userDetails",{deviceID:id},function(err,result){

//         if(err)
//          {
//              res.send({"status":0,"message":"unable to delete this record"});
//          }
//          else{   
//              res.send({"status":0,"message":"Successfully deleted this object"});
//          }

//      });
// });
