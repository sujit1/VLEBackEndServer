
var ObjectID = require('mongodb').ObjectID;
var log4js = require('log4js');
var logger = log4js.getLogger('relative-logger');
var async = require('async');
fs = require('fs')
, Globals = require('../globals')
,_ = require('underscore');


var AWS = require('aws-sdk');
var myConfig = new AWS.Config();
var s3 = new AWS.S3();
myConfig.update({accessKeyId:Globals.accessKeyId,secretAccessKey:Globals.secretAccessKey,"region":Globals.region});

module.exports = organization;

function organization(organization_name,emailId,password,phone_number,organization_location,isOrganization,isAuthenticated,images){
this.organization_name = organization_name;
this.emailId = emailId.toString().toLowerCase();
this.password= [];
this.phone_number = phone_number;
this.organization_location ={
                    latitude:organization_location.latitude,
                    longitude:organization_location.longitude
                    };
this.isOrganization=isOrganization;
this.isAuthenticated= isAuthenticated;
this.OTP= String;
this.loginNumber=0;
if (images) {
  
this.images = {
    "path":images.path,
    "ContentType":images.ContentType,
    "fileName":images.fileName,
    "image":saveImage(images,function(err,result){
        if(err) {
             logger.info(err)
            // callback(err);
            return null;
            }
        else
            {
                //return result.toString();
                console.log(result);
               // callback(null,result)
               var obj= {
                "images":result
            }
               //this.images = result
               return obj;

            }
    })
}
}
};


function saveImage(panelImage,callback1){

     var filename=panelImage.fileName;
     var contentType= panelImage.ContentType;
    // var data= panelImage.image;
var data1 = JSON.parse(panelImage.image);
    var data= data1;

 var calls=[];
 var ret;
     calls.push(function(callback){

         var myBuffer = new Buffer(data.length);

         var myBuffer = Buffer.from(data);
         fs.writeFile("./tmp/"+filename, myBuffer, function(errWrite) {
           if(errWrite) {
                     return callback(errWrite);
                  }
            else {
                   var myBucket = Globals.S3BucketName;
                   var myKey = filename;

         fs.readFile('./tmp/'+filename, function (errRead, data) {
             if (errRead) {
                 return callback(errRead);
             }
             else
                 {
                     params = {Bucket: myBucket, Key: myKey, Body: data,ACL:"public-read",ContentType:"image/"+contentType};

                                 s3.upload(params, function(errUpload, data) {
                                     if (errUpload) {

                                 fs.unlink('./tmp/'+filename, function(err) {
                                     if (err) console.log(err);

                                 });
                                 return callback(errUpload);
                             } else {
                                           console.log("Successfully uploaded data to myBucket/myKey");
                                           ret=data.Location;
                                           fs.unlink('./tmp/'+filename, function(err) {
                                             if (err) console.log(err);

                                         });

                                           return callback(null,{"path":panelImage.path,"fileName":panelImage.fileName,"ContentType":panelImage.ContentType,"image":data.Location});
                                           }



                                   });
                 }

                   });

               }
           });

     })

     async.parallel(calls, function(err, result) {
         /* this code will run after all calls finished the job or
            when any of the calls passes an error */
         if (err){
              ret=1;
             callback1(err);
         }
         else
         {
             logger.info(result);
             callback1(null,result[0]);
         }


         });

       while(ret === undefined) {
         require('deasync').runLoopOnce();
       }

       return ret;
  }
