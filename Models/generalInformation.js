var ObjectID = require('mongodb').ObjectID;
var log4js = require('log4js');
var logger = log4js.getLogger('relative-logger');
var async = require('async');
fs = require('fs')
, Globals = require('../globals')
,_ = require('underscore');

var AWS = require('aws-sdk');
var myConfig = new AWS.Config();
var s3 = new AWS.S3();
myConfig.update({accessKeyId:Globals.accessKeyId,secretAccessKey:Globals.secretAccessKey,"region":Globals.region});

module.exports = generalInformation;
var wait=require('wait.for');

var ObjectID = require('mongodb').ObjectID;

function generalInformation(assignment_id,categoryId,address,installerName,phone_number,installationDate,latitude,longitude,images,others) {
    // custom type checking here...
   // this.installationId= new ObjectID();
   this.assignment_id=new ObjectID(assignment_id);
    this.categoryId = categoryId;
  //  this.installer_id =   new ObjectID(installer_id);
  //  this.projectId = new ObjectID(projectId);
    this.address=address;
    this.installerName =     installerName;
    this.phone_number =  phone_number;
    this.installationDate =     installationDate;
    this.latitude =  latitude;
    this.longitude =     longitude;
    this.images=[];
    this.others= others;
    this.isActive=1;

  }



  generalInformation.prototype.addImages = function(panelImage) {
    return new Promise(
        function (resolve, reject) {
        saveImage(panelImage,function(err,result){
            if(err) {
                     // callback(err);
                     reject(err);
                    }
            else
                {
                    //return result.toString();  
                    //callback(null,result)       
                    resolve(result);                                   
                    
                }
        })
    }
    )
    
    };

    

  function saveImage(panelImages,callback){
    var calls=[];
 
    var ret;
    _.each(panelImages,function(panelImage){
     // var filename=panelImage.filename;
     // var contentType=panelImage.contentType;

     var data1 = JSON.parse(panelImage.image);
     var data= data1;

     calls.push(function(callback1){        
         var myBuffer = Buffer.from(data);
         fs.writeFile("./tmp/"+panelImage.fileName, myBuffer, function(errWrite) {
           if(errWrite) {
                     return callback1(errWrite);
                  } 
            else {
                   
         fs.readFile('./tmp/'+panelImage.fileName, function (errRead, data) {
             if (errRead) {
                 return callback1(errRead);
             }
             else
                 {
                  var myBucket = Globals.S3BucketName;
                 // var myKey = panelImage.filename;
                 
                    var  params = {
                      Bucket:myBucket,
                      Key:panelImage.fileName,
                      Body: data,
                      ACL:"public-read",
                      ContentType:"image/"+panelImage.ContentType
                    };
                     
                                 s3.upload(params, function(errUpload, data) {
                                     if (errUpload) {
                                
                                 fs.unlink('./tmp/'+panelImage.fileName, function(err) {
                                     if (err) console.log(err);
                                     
                                 });
                                 return callback1(errUpload);
                             } else {
                                           console.log("Successfully uploaded data to myBucket/myKey");
                                           ret=data.Location;    
                                           fs.unlink('./tmp/'+panelImage.fileName, function(err) {
                                             if (err) console.log(err);
                                             
                                         });
                                      
                                           return callback1(null,{"path":panelImage.path,"fileName":panelImage.fileName,"ContentType":panelImage.ContentType,"image":data.Location});                      
                                           }
                                   });
                 }
                   
                   });
     
               }
           });
 
     })
      
    });
     async.parallel(calls, function(err, result) {
         /* this code will run after all calls finished the job or
            when any of the calls passes an error */
         if (err)
             callback(err);
              logger.info(result);
         callback(null,result);
    
         });
 while(ret === undefined) {
      require('deasync').runLoopOnce();
    }

   // return ret; 
     
  }
