
var ObjectID = require('mongodb').ObjectID;
var log4js = require('log4js');
var logger = log4js.getLogger('relative-logger');
var async = require('async');
fs = require('fs')
, Globals = require('../globals')
,_ = require('underscore');


var AWS = require('aws-sdk');
var myConfig = new AWS.Config();
var s3 = new AWS.S3();
myConfig.update({accessKeyId:Globals.accessKeyId,secretAccessKey:Globals.secretAccessKey,"region":Globals.region});


module.exports = project;

function project(organization_Id,project_name,project_description,time_stamp,images,installer_id){
this.organization_Id =  new ObjectID(organization_Id);
this.project_name = project_name.toString().toLowerCase();
this.project_description=project_description;
this.time_stamp = time_stamp;
this.images = null;
var array =[];
_.each(installer_id,function(values){

        array.push({
            installer_id:values,
            installerIsActive:1
        });

});


this.installer_id = array;
this.projectIsActive=1;
}

project.prototype.addImages = function(project_logo,callback) {

if(project_logo)
{

    saveImage(project_logo,function(err,result){
        if(err) {
             logger.info(err)
             callback(err);
            }
        else
            {
                //return result.toString();
                console.log(result);
                callback(null,result)

            }
    })

}
else
{
    var array=[];
    callback(null,null);

}
};

function saveImage(panelImage,callback1){

    var filename=panelImage.fileName;
    var contentType= panelImage.ContentType;
   // var data= panelImage.image;
var data1 = JSON.parse(panelImage.image);
    var data= data1;

var calls=[];
var ret;
    calls.push(function(callback){

        var myBuffer = new Buffer(data.length);

        var myBuffer = Buffer.from(data);
        fs.writeFile("./tmp/"+filename, myBuffer, function(errWrite) {
          if(errWrite) {
                    return callback(errWrite);
                 }
           else {
                  var myBucket = Globals.S3BucketName;
                  var myKey = filename;

        fs.readFile('./tmp/'+filename, function (errRead, data) {
            if (errRead) {
                return callback(errRead);
            }
            else
                {
                    params = {Bucket: myBucket, Key: myKey, Body: data,ACL:"public-read",ContentType:"image/"+contentType};

                                s3.upload(params, function(errUpload, data) {
                                    if (errUpload) {

                                fs.unlink('./tmp/'+filename, function(err) {
                                    if (err) console.log(err);

                                });
                                return callback(errUpload);
                            } else {
                                          console.log("Successfully uploaded data to myBucket/myKey");
                                          ret=data.Location;
                                          fs.unlink('./tmp/'+filename, function(err) {
                                            if (err) console.log(err);

                                        });

                                          return callback(null,{"path":panelImage.path,"fileName":panelImage.fileName,"ContentType":panelImage.ContentType,"image":data.Location});
                                          }



                                  });
                }

                  });

              }
          });

    })

    async.parallel(calls, function(err, result) {
        /* this code will run after all calls finished the job or
           when any of the calls passes an error */
        if (err){
             ret=1;
            callback1(err);
        }
        else
        {
            logger.info(result);
            callback1(null,result[0]);
        }


        });

      while(ret === undefined) {
        require('deasync').runLoopOnce();
      }

      return ret;
 }
