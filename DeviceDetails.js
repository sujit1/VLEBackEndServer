var generalInformation = require('./Models/generalInformation');
var panelInformation = require('./Models/panelInformation');
var batteryInformation = require('./Models/batteryInformation');
var chargeControllerInformation = require('./Models/chargeControllerInformation');
var luminareInformation = require('./Models/luminareInformation');

var loadInformation = require('./Models/loadInformation');
var inverterInformation = require('./Models/inverterInformation');

var Globals = require('./globals');
var async = require('async');

      function deviceDetail(generalInformation,panelInformation,batteryInformation,chargeControllerInformation,luminareInformation,loadInformation,inverterInformation)
     {
this.generalInformation = generalInformation;
this.panelInformation = panelInformation;
this.batteryInformation = batteryInformation;
this.chargeControllerInformation = chargeControllerInformation;
this.luminareInformation = luminareInformation;
this.loadInformation = loadInformation;
this.inverterInformation = inverterInformation
      }


      function DeviceDetails(details,callback)
      {

		     // console.log("Detail of installations are " +JSON.stringify(details));
      
        var err =false;
        var ret;
        
        var array =[];
        var infoOfGeneralInformation = new generalInformation(details.generalInformation.assignment_id,details.generalInformation.categoryId,details.generalInformation.address,details.generalInformation.installerName,details.generalInformation.phone_number,details.generalInformation.installationDate,details.generalInformation.latitude,details.generalInformation.longitude,details.generalInformation.others);
   if(details.generalInformation.images.length>0){
          var general= infoOfGeneralInformation.addImages(details.generalInformation.images).then(function(result){
            return new Promise(
              function (resolve, reject) {
               
                infoOfGeneralInformation.images = result              
               resolve(infoOfGeneralInformation)
                //return infoOfPanelInformation;
              
              }
  
            )
          }).catch(function(err){
            err = true;
            //return 1;
          })
  
      array.push(general);
  
          }
          else
          {
            array.push(1);
            
          }

       // var infoOfPanelInformation = new panelInformation(details.panelInformation.projectMake,details.panelInformation.modelNumber, details.panelInformation.dateOfPurchase,details.panelInformation.warrantyExpiryDate,details.panelInformation.maxRatedPower,details.panelInformation.images,details.panelInformation.others);
               var infoOfPanelInformation = new panelInformation(details.panelInformation.projectMake,details.panelInformation.vendorName,details.panelInformation.modelNumber, details.panelInformation.dateOfPurchase,details.panelInformation.warrantyExpiryDate,details.panelInformation.openCircuitVoltage,details.panelInformation.shortCircuitCurrent, details.panelInformation.maximumPowerPointVoltage,details.panelInformation.maximumPowerPointCurrent,details.panelInformation.moduleEfficiency,details.panelInformation.nominalVoltage,details.panelInformation.maxRatedPower,details.panelInformation.images,details.panelInformation.others);

        if(details.panelInformation.images.length>0){
        var panel= infoOfPanelInformation.addImages(details.panelInformation.images).then(function(result){
          return new Promise(
            function (resolve, reject) {
             
              infoOfPanelInformation.images = result              
             resolve(infoOfPanelInformation)
              //return infoOfPanelInformation;
            
            }

          )
        }).catch(function(err){
          err = true;
          //return 1;
        })

    array.push(panel);

        }
        else
        {
          array.push(1);
          
        }
        
         // var infoOfBatteryInformation = new batteryInformation(details.batteryInformation.projectMake, details.batteryInformation.modelNumber, details.batteryInformation.dateOfPurchase,details.batteryInformation.batteryType,details.batteryInformation.nominalVoltage,details.batteryInformation.capacity,details.batteryInformation.minimumChargingVoltage,details.batteryInformation.maximumChargingVoltage,details.batteryInformation.others);
                  var infoOfBatteryInformation = new batteryInformation(details.batteryInformation.projectMake, details.batteryInformation.modelNumber,details.batteryInformation.vendorName,details.batteryInformation.dateOfPurchase,details.batteryInformation.warrantyExpiryDate,details.batteryInformation.batteryType,details.batteryInformation.nominalVoltage,details.batteryInformation.capacity,details.batteryInformation.minimumChargingVoltage,details.batteryInformation.maximumChargingVoltage,details.batteryInformation.others);

          if(details.batteryInformation.images.length>0){

          //  infoOfBatteryInformation.addImages(details.batteryInformation.batteryImage);
        var battery=    infoOfBatteryInformation.addImages(details.batteryInformation.images).then(function(result){
          return new Promise(
            function (resolve, reject) {
             
              infoOfBatteryInformation.images = result;           
             resolve(infoOfBatteryInformation)
              //return infoOfPanelInformation;
            
            }

          )
        }).catch(function(err){
          err = true;
          //return 1;
        })
        array.push(battery);
          }
          


          if(infoOfGeneralInformation.categoryId==Globals.categorySolarStreetLight|| infoOfGeneralInformation.categoryId==Globals.categorySolarHomeLight)
          {
      //      var infoOfChargeControllerInformation =   new chargeControllerInformation(details.chargeControllerInformation.projectMake, details.chargeControllerInformation.modelNumber, details.chargeControllerInformation.dateOfPurchase,details.chargeControllerInformation.vendorName,details.chargeControllerInformation.warrantyExpiryDate,details.chargeControllerInformation.chargeControllerType,details.chargeControllerInformation.nominalVoltage,details.chargeControllerInformation.maximumInputCurrent,details.chargeControllerInformation.maximumLoadCurrent,details.chargeControllerInformation.others);
                    var infoOfChargeControllerInformation =   new chargeControllerInformation(details.chargeControllerInformation.projectMake, details.chargeControllerInformation.modelNumber,details.chargeControllerInformation.vendorName, details.chargeControllerInformation.dateOfPurchase,details.chargeControllerInformation.warrantyExpiryDate,details.chargeControllerInformation.chargeControllerType,details.chargeControllerInformation.nominalVoltage,details.chargeControllerInformation.maximumInputCurrent,details.chargeControllerInformation.maximumLoadCurrent,details.chargeControllerInformation.others);
    
              if(details.chargeControllerInformation.images.length>0){
                //infoOfChargeControllerInformation.addImages(details.chargeControllerInformation.chargerControllerImage);

              var charger=  infoOfChargeControllerInformation.addImages(details.chargeControllerInformation.images).then(function(result){
                return new Promise(
                  function (resolve, reject) {
                   
                    infoOfChargeControllerInformation.images = result;         
                    resolve(infoOfChargeControllerInformation)
                  
                  }
      
                )
              }).catch(function(err){
                err = true;
                //return 1;
              })

              array.push(charger)
                
              }
              else
              {
                array.push(1);
                
              }
          }
        

          if(infoOfGeneralInformation.categoryId==Globals.categorySolarStreetLight|| infoOfGeneralInformation.categoryId==Globals.categorySolarLantern)
            {
           //   var infoOfLuminareInformation = new luminareInformation(details.luminareInformation.projectMake, details.luminareInformation.modelNumber, details.luminareInformation.dateOfPurchase,details.luminareInformation.vendorName,details.luminareInformation.warrantyExpiryDate,details.luminareInformation.luminaireType,details.luminareInformation.nominalVoltage,details.luminareInformation.maximumInputCurrent,details.luminareInformation.images,details.luminareInformation.others);
                           var infoOfLuminareInformation = new luminareInformation(details.luminareInformation.projectMake, details.luminareInformation.modelNumber,details.luminareInformation.vendorName, details.luminareInformation.dateOfPurchase,details.luminareInformation.warrantyExpiryDate,details.luminareInformation.luminaireType,details.luminareInformation.nominalVoltage,details.luminareInformation.maximumOperatingPower,details.luminareInformation.images,details.luminareInformation.others);
 
                if(details.luminareInformation.images.length>0){
                  //  infoOfLoadInformation.addImages(details.loadInformation.loadImage);

                 var luminar=   infoOfLuminareInformation.addImages(details.luminareInformation.images).then(function(result){

                    return new Promise(
                      function(resolve,reject){
                        infoOfLuminareInformation.images = result;
                        resolve(result);

                      }

                    )
                 }).catch(function(err){
                  err = true;

                 })
                  array.push(luminar);
                  }
                else
                {
                  array.push(1);
                  
                }
            }

            if(infoOfGeneralInformation.categoryId==Globals.categorySolarHomeLight){
              var infoOfLoadInformation = new loadInformation(details.loadInformation.loadType,details.loadInformation.projectMake,details.loadInformation.nominalVoltage,details.loadInformation.dateOfPurchase,details.loadInformation.warrantyExpiryDate,details.loadInformation.maxRatedPower,details.loadInformation.images,details.loadInformation.others);
              if(details.loadInformation.images.length>0){
              //  infoOfLoadInformation.addImages(details.loadInformation.loadImage);
             var load=   infoOfLoadInformation.addImages(details.loadInformation.images).then(function(result){
              
                                  return new Promise(
                                    function(resolve,reject){
                                      infoOfLoadInformation.images = result;
                                      resolve(infoOfLoadInformation);
              
                                    }
              
                                  )
                               }).catch(function(err){
                                err = true;
              
                               })

              array.push(load);
              }
              else
              {
                array.push(1);
                
              }

            }

          if(infoOfGeneralInformation.categoryId==Globals.categorySolarInverterSystem){
            var infoOfInverterInformation = new inverterInformation(details.inverterInformation.projectMake,details.inverterInformation.inverterModelNumber,details.inverterInformation.vendorName,details.inverterInformation.dateOfPurchase,details.inverterInformation.warrantyExpiryDate,details.inverterInformation.voltage,details.inverterInformation.images,details.inverterInformation.others);
            if(details.inverterInformation.images.length>0){
             // infoOfInverterInformation.addImages(details.inverterInformation.inverterImage);
              var inverter=   infoOfInverterInformation.addImages(details.inverterInformation.images).then(function(result){
                
                                    return new Promise(
                                      function(resolve,reject){
                                        infoOfInverterInformation.images = result;
                                        resolve(infoOfInverterInformation);
                
                                      }
                
                                    )
                                 }).catch(function(err){
                                  err = true;
                
                                 })
            array.push(inverter);

            }
            else
            {
              array.push(1);
              
            }
         
          }

           
    Promise.all(array).then(function(data) {
          

            
              ret =1;
              if( err == true)
              {
                callback({"status":1,"message":"Unable to insert the record"});
              }else
              {
                callback(null, new deviceDetail(infoOfGeneralInformation,infoOfPanelInformation,infoOfBatteryInformation,infoOfChargeControllerInformation,infoOfLuminareInformation,infoOfLoadInformation,infoOfInverterInformation));
                
              }  

        });


        while(ret === undefined) {
          require('deasync').runLoopOnce();
        }
        
   // return ret;

            /* this code will run after all calls finished the job or
               when any of the calls passes an error */        

       
           // var success = results.filter(x => x.status === "resolved");


      }
      

      module.exports = DeviceDetails;
      
