var Globals = require('../globals')
, mongodb = require('../db')
_ = require('underscore')
, ObjectID = require('mongodb').ObjectID;

var DeviceDetails = require('../DeviceDetails');
var async = require('async');


exports.getAllInstallations = function(callback){
    mongodb.findAll(Globals.InstallationCollectionName,function(err,result){
        
                if(err)
                 {
                     res.send({"status":0,"message":"error in retriving the record"});
                 }
                 else{
                    if(result.length>0)
                        {
                     res.json(result); 
                        }
                        else
                            {
                                res.send({"status":0,"message":"No installations found"});
                            }
        
                 }
        
             });
}

exports.addNewInstallation = function(installationArray,callback){
    var successArray=[];
    var failurArray =[];
    var calls = [];



    var promises=installationArray.map(function(installationDetails){
        var callbackCalled = false;
        if(installationDetails.generalInformation.categoryId==Globals.categorySolarStreetLight|| installationDetails.generalInformation.categoryId==Globals.categorySolarHomeLight)
            {
                if(!installationDetails.chargeControllerInformation)
                    {
                        //callback({"status":1,"message":"Charger controller information is not available please enter the charger controller information"});
                        failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Charger controller information is not available please enter the charger controller information"});
                        callbackCalled=true;
                    }
            }
        if(installationDetails.generalInformation.categoryId==Globals.categorySolarStreetLight|| installationDetails.generalInformation.categoryId==Globals.categorySolarLantern)
            {
                if(!installationDetails.luminareInformation){
                   // callback({"status":1,"message":"Luminare information is not available please enter the Luminare information"});
                   failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Luminare information is not available please enter the Luminare information"});
                    callbackCalled=true;
                }
                
            }
            if(installationDetails.generalInformation.categoryId==Globals.categorySolarHomeLight){
                if(!installationDetails.loadInformation){
                   // callback({"status":1,"message":"Load information is not available please enter the Load information"});
                   failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Load information is not available please enter the Load information"});
                    callbackCalled=true;
                }
            }
            if(installationDetails.generalInformation.categoryId==Globals.categorySolarInverterSystem){
                if(!installationDetails.inverterInformation)
                    {
                       // callback({"status":1,"message":"Inverter information is not available please enter the Inverter information"});
                       failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Inverter information is not available please enter the Inverter information"});
                       
                        callbackCalled=true;                    
                        
                    }
            }
    
    if(callbackCalled==false)
    {
        var installation;
         new DeviceDetails(installationDetails,function(err,DeviceDetails){
            if(err) {
                console.log('Error' +err);
                 installation= null;
            }
            else
                {
                    installation= DeviceDetails;
                }
    
        });
    
    
        if(installation)
            {
            successArray.push(installation);
           // callback(installation); 
            
            return installation
            
            }
        else
            {
                    failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Unable to Inset the record"});                
                   // callback(1);
                    return 1
                    
            }
    
               
            
    
        }
        else
            {
               // callback(1);
                return 1;
            }
          
    })




//    _.each(installationArray,function(installationDetails){
//      calls.push(function(callback){

  
        
// });

// })


Promise.all(promises).then(function (albums) { // albums is 2D array
    
    if(successArray.length>0){
        
            mongodb.saveMany(Globals.InstallationCollectionName,successArray,function(errSave,result){
                if (errSave) callback(errSave);
                else
                    {
				  var arrayResult=[];
                        arrayResult.push({"success installations":result});
                        arrayResult.push({"failure installations":failurArray});

                        callback(null,arrayResult); 
                        //callback(null,{"success installations":result,"failure installations":failurArray}); 
                    }
            })
        }
        else
            {
		  var arrayResult=[];
                        arrayResult.push({"success installations":null});
                        arrayResult.push({"failure installations":failurArray});

                        callback(null,arrayResult); 
                //callback(null,{"success installations":successArray,"failure installations":failurArray});        
            }

    //do something with the finalized list of albums here
});

   
}

exports.viewAllInstallations = function(projectDetails,callback){

   
mongodb.findByObjects(Globals.AssignmentsCollectionName,{"_id":ObjectID(projectDetails.assignment_id)},function(errInProject,resultOfProject){
    if(errInProject){
        callback(errInProject)
    }
    else
    {
            if(resultOfProject.length>0){

                var arrayOfIds=[];
                _.each(resultOfProject,function(values){
                    arrayOfIds.push(ObjectID(values._id));
                })
                var query={
                    "generalInformation.assignment_id":{"$in":arrayOfIds},
                    "generalInformation.isActive":1
                }            
                mongodb.findByObjects(Globals.InstallationCollectionName,query,function(err,result){
                    if(err){
                        callback(err)
                    }
                    else
                    {
                        
                        if(result.length>0){
                            callback(null,{"status":0,"message":"Successfully got the records","result":result})
                        }
                        else
                        {
                            callback(null,{"status":1,"message":"No installations found in this project"});
                        }
                    }
            
                })
            }
            else
            {
                callback(null,{"status":1,"message":"Unable to find the project details, please check your project details"});
            }
    }

})

  
}


exports.updateInstallation = function(installationArray,callback){
    var successArray=[];
    var failurArray =[];
    var calls = [];

var arrayOfIdsToUpdate=[];

    var promises=installationArray.map(function(installationDetails){
        var callbackCalled = false;

        arrayOfIdsToUpdate.push(new ObjectID(installationDetails._id));

        
        if(installationDetails.generalInformation.categoryId==Globals.categorySolarStreetLight|| installationDetails.generalInformation.categoryId==Globals.categorySolarHomeLight)
            {
                if(!installationDetails.chargeControllerInformation)
                    {
                        //callback({"status":1,"message":"Charger controller information is not available please enter the charger controller information"});
                        failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Charger controller information is not available please enter the charger controller information"});
                        callbackCalled=true;
                    }
            }
        if(installationDetails.generalInformation.categoryId==Globals.categorySolarStreetLight|| installationDetails.generalInformation.categoryId==Globals.categorySolarLantern)
            {
                if(!installationDetails.luminareInformation){
                   // callback({"status":1,"message":"Luminare information is not available please enter the Luminare information"});
                   failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Luminare information is not available please enter the Luminare information"});
                    callbackCalled=true;
                }
                
            }
            if(installationDetails.generalInformation.categoryId==Globals.categorySolarHomeLight){
                if(!installationDetails.loadInformation){
                   // callback({"status":1,"message":"Load information is not available please enter the Load information"});
                   failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Load information is not available please enter the Load information"});
                    callbackCalled=true;
                }
            }
            if(installationDetails.generalInformation.categoryId==Globals.categorySolarInverterSystem){
                if(!installationDetails.inverterInformation)
                    {
                       // callback({"status":1,"message":"Inverter information is not available please enter the Inverter information"});
                       failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Inverter information is not available please enter the Inverter information"});
                       
                        callbackCalled=true;                    
                        
                    }
            }
    
    if(callbackCalled==false)
    {
        var installation;
         new DeviceDetails(installationDetails,function(err,DeviceDetails){
            if(err) {
                console.log('Error' +err);
                 installation= null;
            }
            else
                {
                    installation= DeviceDetails;
                }
    
        });
    
    
        if(installation)
            {
            successArray.push(installation);
           // callback(installation); 
            
            return installation
            
            }
        else
            {
                    failurArray.push({"installationId":installationDetails.generalInformation.installationId,"Error":"Unable to Inset the record"});                
                   // callback(1);
                    return 1
                    
            }
    
               
            
    
        }
        else
            {
               // callback(1);
                return 1;
            }
          
    })




//    _.each(installationArray,function(installationDetails){
//      calls.push(function(callback){

  
        
// });

// })


Promise.all(promises).then(function (albums) { // albums is 2D array
    
    if(successArray.length>0){
        
//////////////////////////////////////////////////////////////////////We can update here ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//exports.updateManyByObjects= function(collection,conditionalObjects,setObjects,callback){
    


            mongodb.updateManyByObjects(Globals.InstallationCollectionName,{_id:{$in:arrayOfIdsToUpdate}},{$set:successArray[0]}  ,function(errSave,result){
                if (errSave) callback(errSave);
                else
                    {
                        if(result.result.n>0){
                     var array=[];
                     
                     array.push({"status":"0","message":"Successfully updated"})
                     callback(null,array);
                        }
                        else
                        {
			var array=[];
            array.push({"status":"1","message":"Unable to update please try again later"})                     
                   callback(null,array); 
                        }
                    }
            })


        }
        else
            {
                var arrayResult=[];
               // arrayResult.push({"success installations":result});
                arrayResult.push({"failure installations":failurArray});

                callback(null,arrayResult);        
            }

    //do something with the finalized list of albums here
});

   
}

exports.deleteInstallations = function(installationId,callback){
    
        var query ={
            _id:new ObjectID(installationId._id)
        }
    mongodb.findByObjects(Globals.InstallationCollectionName,query,function(err,installationsResult){
        if(err) callback(err)
        else
        {
            if(installationsResult.length>0){
    
                mongodb.updateManyByObjects(Globals.InstallationCollectionName,{"_id":installationsResult[0]._id},{$set:{"generalInformation.isActive":0} },function(errRe,updatedRes){
                    if(errRe) callback(errRe)
                    else
                    {
                        if(updatedRes.result.n>0){
    
                            callback(null,{"status":"0","message":"Successfully updated"});
                        }
                        else
                        {
                            callback(null,{"status":"1","message":"Unable to  updated please try again later"});
                            
                        }
                        
                    }
                })
    
            }
            else
            {
                callback(null,{"status":1,"message":"No installations found"});
                
            }
        }
    })
    
    }