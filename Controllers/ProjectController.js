var express = require('express')
, router = express.Router()
, bcrypt = require('bcrypt')
, log4js = require('log4js')
, Globals = require('../globals')
, mongodb = require('../db')
, ObjectID = require('mongodb').ObjectID
,_ = require('underscore');


var project = require('../Models/Project');



exports.addNewProject = function(organizationDetails,callback){

var query = {
    project_name:organizationDetails.project_name.toString().toLowerCase(),
    organization_Id: new ObjectID(organizationDetails.organization_Id)
};
    mongodb.findByObjects(Globals.ProjectCollectionName,query,function(errInOrg,result){

        if(errInOrg) callback(errInOrg);
        else
            {
                if(result.length>0)
                    {
                        callback(null,{"status":1,"message":"Project already exist with this name.Please try with another name"});

                    }
                    else
                        {
                            var projectDetails = new project(organizationDetails.organization_Id,organizationDetails.project_name,organizationDetails.project_description,organizationDetails.time_stamp,organizationDetails.images,organizationDetails.installer_id);
                            projectDetails.addImages(organizationDetails.images,function(err,result){
                                if(err){
                                    callback(err)
                                }
                                else
                                    {

                                        // for(var i=0;i<result.installer_id.length;i++){
                                        //     arrayOfInstallers.push(ObjectID(result.installer_id[i]));
                                        // }


                                        var arrayOfInstallers=[];
                                        _.each(organizationDetails.installer_id,function(values){
                                            arrayOfInstallers.push(values);
                                        })


                                        var query={
                                            _id:{$in:arrayOfInstallers}
                                        }

                                        mongodb.findByObjectsWithFields('Users',query,{installer_first_name:1,installer_last_name:1,emailId:1},function(errInstaller,installers){
                                            if(errInstaller) callback(err);
                                            else
                                            {
                                                if(result){
                                                    projectDetails.images=result

                                                }
                                                else
                                                {
                                                    projectDetails.images=null;
                                                }



                                                mongodb.save(Globals.ProjectCollectionName,projectDetails,function(err,projects){
                                                    if(err){
                                                        callback(err);
                                                    }
                                                    else
                                                        {
                                                           // callback(null,projects);
                                                            projects.installer_id=[];
                                                            projects.installer_id =installers;

                                                 //  callback(null,projects);
                                                   callback(null,{"status":0,"message":"Successfully added project to organization","result":projects})
                                                        }

                                                });



                                            }
                                        })


                                 }

                            });


                        }

            }

    });





}



exports.updateProject= function(organizationDetails,callback){
    mongodb.findById(Globals.ProjectCollectionName,organizationDetails._id,function(err,findResult){
        if(err){
            callback(err);
        }
        else
        {
           if(findResult){
            var projectDetails = new project(organizationDetails.organization_Id,organizationDetails.project_name,organizationDetails.project_description,organizationDetails.time_stamp,organizationDetails.images,organizationDetails.installer_id);
            projectDetails.addImages(organizationDetails.images,function(err,result){
                if(err){
                    callback(err)
                }
                else
                    {
                                if(result){
                                    projectDetails.images=result
                                    
                                }
                                else
                                {
                                    projectDetails.images=null;
                                }
                                delete projectDetails.installer_id;
                                mongodb.updateById(Globals.ProjectCollectionName,findResult._id,{$set:projectDetails},function(updateError,projects){
                                    if(err){
                                        callback(err);
                                    }
                                    else
                                        {

                                            if(projects.result.n>0)
                                            {
                                                callback(null,{"status":0,"message":"Successfully updated project details"})
                                                
                                            }
                                            else
                                            {
                                                callback({"status":1,"message":"Unable to update your details Please try again later"})
                                                
                                            }
                                      
                                        }
                               
                                });

                 }
                
            });
             
           
        }
           else
           {

           }
        }

    })
}



exports.addInstallersToProject= function(details,callback){

    mongodb.findById(Globals.ProjectCollectionName,details._id,function(err,result){
        if(err){
            callback(err);
        }
        else
        {
            if(result){

                if(result.installer_id.length<1 && details.installer_id.length<1){
                    callback(null,{"status":1,"message":"Please select installlers and try again"})
                }
                else if(details.installer_id.length>0 )
                {

                
                var arrayOfInstallers=[];
                _.each(details.installer_id,function(values){
                    arrayOfInstallers.push(new ObjectID(values));
                })

                var query={
                    _id:{$in:arrayOfInstallers}

                }

                mongodb.findByObjectsWithFields('Users',query,{installer_first_name:1,installer_last_name:1,emailId:1},function(errInstaller,installers){
                    if(errInstaller) callback(err);
                    else
                    {
                        
                        if(installers.length>0){

var arrayM=[];
var arrayUM=[];

                            var promises=[];
                            _.each(installers,function(values,dx){
                                promises.push(new Promise(function(resolve,reject){
                             
                                var query={
                                    _id:result._id,
                                    "installer_id.installer_id":values._id
                                }

                                mongodb.findByObjects(Globals.ProjectCollectionName,query,function(err,receivedDetails){
                                    if(err){
                                        reject(err);
                                    }
                                    else
                                    {
                                        if(receivedDetails.length>0){
                                            arrayM.push(values._id.toString());
                                            resolve(values._id);
                                        }
                                        else{
                                            arrayUM.push(values._id.toString());
                                            resolve(values._id);
                                            
                                        }

                                    }

                                })


                            }))

                            })





                            // _.each(installers,function(values,dx){

                            //     promises.push(new Promise(function(resolve,reject){

                            //         var arrayOfInstallersInsideDB=[];
                                    
                            //                                         var objToAddInstallerToProject={
                            //                                             installer_id:values._id,
                            //                                             installerIsActive: details.installer_id[dx].installerIsActive
                            //                                         }
                                    
                            //                                         var queryObjectInsert;
                            //                                         var conditionQuery;
                                    
                            //                                         if(details.installer_id[dx].installerIsActive==true){
                            //                                             queryObjectInsert={
                            //                                                 $push:{installer_id:objToAddInstallerToProject}
                            //                                             }
                            //                                             conditionQuery={
                            //                                                 _id:result._id
                            //                                             }
                            //                                         }
                            //                                         else
                            //                                         {
                            //                                             queryObjectInsert={
                            //                                                 $pull:{"installer_id":{ "installer_id" :values._id} }
                            //                                             }
                            //                                             conditionQuery={
                            //                                                 _id:result._id,
                            //                                                 "installer_id.installer_id":values._id
                            //                                             }
                                                                        
                            //                                         }
                                                                    
                                    
                            //                                         mongodb.updateManyByObjects(Globals.ProjectCollectionName,conditionQuery,queryObjectInsert,function(errAdding,resultOfProject){
                            //                                             if(errAdding)  reject(errAdding);
                            //                                             else
                            //                                             {
                            //                                                 if(resultOfProject.result.n>0){
                            //                                                    // callback(null,installers);
                                        
                            //                                                    //callback(null,{"status":0,"message":"Successfully added instalers to your project"});
                                    
                                    
                                    
                            //                                         if(details.installer_id[dx].installerIsActive==true){
                                                                        
                            //                                                      var obj={
                            //                                                             projectId:new ObjectID(details._id),
                            //                                                             organization_Id :new ObjectID(details.organization_Id),
                            //                                                             installer_id:new ObjectID(values._id),
                            //                                                             isActive:true 
                            //                                                         }
                                                                                    
                            //                                                 //    var Assignments = new assignments(projects.organization_Id,projects.installer_id,projects._id.toString());
                                                                              
                            //                                                     mongodb.save(Globals.AssignmentsCollectionName,obj,function(errInAss,AssResult){
                            //                                                         if(errInAss) reject(errInAss);
                            //                                                         else
                            //                                                         {
                            //                                                             resolve({"status":0,"message":"Successfully added instalers to your project","result":AssResult});
                            //                                                         }
                            //                                                     })
                                    
                            //                                         }
                            //                                         else
                            //                                         {
                            //                                             var obj={
                            //                                                 projectId:new ObjectID(details._id),
                            //                                                 organization_Id :new ObjectID(details.organization_Id),
                            //                                                 installer_id:new ObjectID(values._id),
                            //                                             }
                                    
                            //                                         mongodb.updateManyByObjects(Globals.AssignmentsCollectionName,obj,{$set:{isActive:false}},function(errAdding,resultOfAssign){
                            //                                             if(errAdding)  reject(errAdding);
                            //                                             else{
                            //                                                 if(resultOfAssign.result.n>0)
                            //                                                 {
                                    
                            //                                                     resolve({"status":0,"message":"Successfully added instalers to your project"});
                                                                                
                            //                                                 }
                            //                                                 else
                            //                                                 {
                            //                                                     reject({"status":1,"message":"Unable to add installer to this project"});
                                                                                
                            //                                                 }
                                    
                            //                                             }
                            //                                         })
                                    
                                    
                            //                                         }
                                    
                            //                                                 }else
                            //                                                 {
                            //                                                     reject({"status":1,"message":"Unable to add installer to this project"});
                            //                                                 }
                            //                                             }
                            //                                         })
                                        





                            //     } ))
                              
                                
                            // })

                            Promise.all(promises).then(function(data){
                                var idsToRemove=[];
                                var arrayOfValuesFromDBToCompare=[];
                                _.each(result.installer_id,function(unMatchedValues){
                                    arrayOfValuesFromDBToCompare.push(unMatchedValues.installer_id.toString())
                                })


                                var arrayToRemove=[];
                               //arrayToRemove =arrayM.diff(arrayOfValuesFromDBToCompare) 

                               arrayToRemove = arrayM.filter(function(entry1) {
                                return arrayOfValuesFromDBToCompare.some(function(entry2) { 
                                    return entry1 === entry2; 
                                })
                            });

                            var removingIdsFromDB=arrayOfValuesFromDBToCompare.filter(function(e){return this.indexOf(e)<0;},arrayToRemove);
 
                            var numberOfLoops=arrayM.length+arrayUM.length+removingIdsFromDB.length;
                            var handledPromises=[]
                          var arrayOfAssignments=[];
                          var finalAssignmentArray=[];
                            var secondAssignmentArray=[]
                          
                               handledPromises.push(new Promise(function(resolve,reject){
                                if(arrayUM.length>0){
                                    var arrayForAddingInstaller=[];
                                    var arrayOfInstallerForAssign=[];
                                    _.each(arrayUM,function(addingInstaller){
                                        var objToAddInstallerToProject={
                                            installer_id:new ObjectID(addingInstaller),
                                            installerIsActive: 1
                                      }
                                      arrayForAddingInstaller.push(objToAddInstallerToProject)

                                      var obj={
                                        projectId:new ObjectID(result._id),
                                       organization_Id :new ObjectID(details.organization_Id),
                                       installer_id:new ObjectID(addingInstaller),
                                       isActive:1 
                                           }
                                     arrayOfInstallerForAssign.push(obj);


                                    })
                                   

                                    mongodb.updateManyByObjects(Globals.ProjectCollectionName,{_id:result._id},{$pushAll:{installer_id:arrayForAddingInstaller}},function(errAdding,successDetails){
                                                             if(errAdding)  reject(errAdding);
                                                             else
                                                             {
                                                                if(successDetails.result.n>0){

                                                                mongodb.saveMany(Globals.AssignmentsCollectionName,arrayOfInstallerForAssign,function(errInAss,AssResult){
                                                                     if(errInAss) reject(errInAss);
                                                                     else
                                                                    {
                                                                        if(AssResult.length>0)
                                                                        {
                                                                            finalAssignmentArray =  arrayOfAssignments.concat(AssResult);


                                                                            resolve(AssResult);
                                                                        }
                                                                        //resolve({"status":0,"message":"Successfully added instalers to your project","result":AssResult});
                                                                     }
                                                                 })

                                                                }
                                                                else
                                                                {
                                                                        reject(1)
                                                                }
                                                            }
                                                                                    
                                                     })

                                                  }
                                                  else
                                                  {
                                                      resolve(1);
                                                  }

                                             }))


                                             handledPromises.push(new Promise(function(resolve,reject){
                    if(arrayM.length>0)
                                                  {
                                                      var arrayInst=[];
                                                      _.each(arrayM,function(values){
                                                        arrayInst.push(new ObjectID(values));
                                                      })
                                                      var queryForAssignments={
                                                        projectId:new ObjectID(result._id),
                                                        organization_Id :new ObjectID(details.organization_Id),
                                                        installer_id:{$in:arrayInst},
                                                        isActive:1 
                                                      }
                                                      mongodb.findByObjects(Globals.AssignmentsCollectionName,queryForAssignments,function(errorAss,AssiIds){
                                                        if(errorAss) reject(errorAss)
                                                        else
                                                        {
                                                            if(AssiIds.length>0){
                                                                secondAssignmentArray =  arrayOfAssignments.concat(AssiIds);
                                                                resolve(AssiIds);
                                                            }
                                                            else{
                                                                resolve(1);
                                                            }
                                                        }
                                                      })
                                                  }
                                                  else
                                                  {
                                                      resolve(1);
                                                  }


                                                }))

                                                  handledPromises.push(new Promise(function(resolve,reject){
                        if(removingIdsFromDB.length>0){
                                        var arrayDeleteInstaller=[];
                                        
                                                                            _.each(removingIdsFromDB,function(deleteValues){
                                                                                arrayDeleteInstaller.push(new ObjectID(deleteValues))
                                                                            })
                                                                                queryObjectInsert={
                                                                                                 $pull:{"installer_id":{ "installer_id" :{$in:arrayDeleteInstaller} } }
                                                                                                }
                                                                                 conditionQuery={
                                                                                                _id:result._id,
                                                                                                "installer_id.installer_id":{$in:arrayDeleteInstaller}
                                                                                                 }
                                                                                        
                                                                                mongodb.updateManyByObjects(Globals.ProjectCollectionName,conditionQuery,queryObjectInsert,function(errAdding,resultOfProject){
                                                                                    if(errAdding)  reject(errAdding);
                                                                                         if(resultOfProject.result.n>0){
                                        
                                                                                            var conditionQuery1={
                                                                                                projectId:new ObjectID(result._id),
                                                                                                organization_Id :new ObjectID(details.organization_Id),
                                                                                                installer_id:{$in:arrayDeleteInstaller },
                                                                                            }
                                                                                            
                                        
                                                                                            mongodb.updateManyByObjects(Globals.AssignmentsCollectionName,conditionQuery1,{$set:{isActive:0} },function(errAdding,resultOfAsss){
                                                                                                if(errAdding)  reject(errAdding);
                                                                                                     if(resultOfAsss.result.n>0){
                                                                                                        resolve(1)
                                                                                                     }
                                                                                                     else{
                                                                                                        reject(1);
                                                                                                      }
                                                    
                                                                                            })
                                        
                                                                                         }
                                                                                         else{
                                                                                            reject(1);
                                                                                          }
                                        
                                                                                })
                                    }
                                    else
                                    {
                                        resolve(1);
                                    }
                                    
                                }))
                                Promise.all(handledPromises).then(function(data){
                                    var arra= finalAssignmentArray.concat(secondAssignmentArray);
                                    callback(null,{"status":0,"message":"Successfully updated your installers in project","result":arra});

                                 }).catch(err,function(){
                                     callback(null,{"status":1,"message":"Unable to find installers Please check your details","error":err });
                                 })
                              
                               
                               // callback(null,{"status":0,"message":"Successfully updated your installers in project","arrayM":arrayM,"arrayUM":arrayUM,"de":removingIdsFromDB });
                                
                            }).catch(function(err){
                                callback(null,{"status":1,"message":"Unable to find installers Please check your details","error":err });
                                
                            })


                        
                        }
                        else
                        {
                            callback(null,{"status":1,"message":"Unable to find installers Please check your details"});
                        }
                        
                       
                    }
                })



            }
            else if(result.installer_id.length>0 && details.installer_id.length<1 ){

                mongodb.updateManyByObjects(Globals.ProjectCollectionName,{_id:result._id},{$set:{installer_id:[]} },function(err,deletingAllRecords){
                    if(err) callback(err)
                    else{
                        if(deletingAllRecords.result.n>0){

                            mongodb.updateManyByObjects(Globals.AssignmentsCollectionName,{projectId:result._id},{$set:{isActive:0}},function(err,deletingAssRec){
                                if(err) callback(err)
                                else
                                {
                                    if(deletingAssRec.result.n>0){
                                        callback(null,{"status":0,"message":"Successfully updated your installers in project","result":null});
                                        
                                    }
                                    else
                                    {
                                        callback(null,{"status":1,"message":"Unable to find installers Please check your details"})
                                        
                                    }
                                }
                            })

                        }
                        else
                        {
                            callback(null,{"status":1,"message":"Unable to find installers Please check your details"})
                        }
                    }
                })

            }

	else
            {
                callback(null,{"status":1,"message":"Unable to find the project Please check your details"});
                
            }
            }
            else
            {
                callback(null,{"status":1,"message":"Unable to find the project Please check your details"});
            }
            
        }

    })

}





exports.allProjectsOfAnOrganization = function(organizationDetails,callback){
    mongodb.findByObjects(Globals.ProjectCollectionName,{organization_Id: new ObjectID(organizationDetails.organization_Id),projectIsActive:1},function(err,result){
        if(err) callback(err);
            else{
                if(result.length>0)
                    {

                        callback(null,{"status":0,"message":"Successfully retrieved the project details","result":result});
                    }
                    else
                        {
                            callback(null,{"status":1,"message":"No Projects are found in this Organization"});
                        }
            }
    });
}



exports.getAllInstallersInProject = function(projectDetails,callback){

    var query ={
        _id:ObjectID(projectDetails._id),
        projectIsActive:1
    }

    mongodb.findByObjects(Globals.ProjectCollectionName,query,function(err,projectDetails){
        if(err) callback(err);
            else{
                if(projectDetails.length>0)
                    {
                        var arrayOfInstallers=[];

                        var result=projectDetails[0];
                        // for(var i=0;i<result.installer_id.length;i++){
                        //     arrayOfInstallers.push(ObjectID(result.installer_id[i]));
                        // }
                        _.each(result.installer_id,function(values){

                            if(values.installerIsActive==1){
                            arrayOfInstallers.push(values.installer_id);
                            }
                        })


                        var query={
                            _id:{$in:arrayOfInstallers}
                        }
                        mongodb.findByObjectsWithFields('Users',query,{installer_first_name:1,installer_last_name:1,emailId:1,_id:1},function(errInstaller,installers){
                      //  mongodb.findByObjects('Users',query,function(err,installers){
                            if(err) callback(err);
                            else
                                {
                                    if(installers.length>0)
                                        {
                                    callback(null,{"status":0,"message":"Successfully retrieved installers","result":installers});
                                        }
                                        else
                                            {
                                                callback(null,{"status":1,"message":"No installers are found in this project"});
                                            }
                                }
                        });
                    }
                    else
                        {
                            callback(null,{"status":1,"message":"No Project found"});
                        }
            }
    });

}

exports.getAllProjectsInstallerInvolvedIn = function(organizationDetails,callback){

    var query = {

        "installer_id.installer_id":new ObjectID(organizationDetails.installer_id),
        "installer_id.installerIsActive":1,
        projectIsActive:1

    }


    mongodb.findByObjectsWithFields(Globals.ProjectCollectionName,query,{project_name:1,project_description:1,time_stamp:1,images:1,_id:1},function(err,result){
        if(err) callback(err);
        else
            {
                if(result.length>0){
                    callback(null,{"status":0,"message":"Successfully retrieved all projects","result":result});

                }
                else
                    {
                        callback(null,{"status":1,"message":"No projects are found for this installer"});

                    }
            }

    });
}


exports.deleteProject = function(projectDetails,callback){


    var arrayOfProjects=[];

                            // for(var i=0;i<details.installer_id.length;i++){
                            //     arrayOfInstallers.push(ObjectID(details.installer_id[i]));
                            // }
                            _.each(projectDetails._id,function(values){
                                arrayOfProjects.push(ObjectID(values))
                            })
                            var query={
                                _id:{$in:arrayOfProjects}
                            }


    mongodb.findByObjects(Globals.ProjectCollectionName,query,function(findObjerr,projects){

        if(findObjerr){
            callback(err);
        }
        else
            {
                if(projects.length>0){

                    mongodb.updateManyByObjects(Globals.ProjectCollectionName,query,{$set:{"projectIsActive":0}},function(err,result)
                    {
                        if (err) callback(err);
                        else
                            {
                                if(result)
                                    {
                                        //callback(null,result);
                                        var queryProjects={
                                            projectId:{$in:arrayOfProjects}
                                        }
                                        mongodb.updateManyByObjects(Globals.AssignmentsCollectionName,queryProjects,{$set:{"isActive":0}},function(errAss,result)
                                        {
                                            if(errAss) callback(errAss)
                                            else
                                            {
                                                if(result)
                                                {
                                                    callback(null,result);

                                                }
                                                else
                                                {
                                                    callback(null);
                                                }
                                            }
                                        })



                                    }
                                    else
                                        {
                                            callback(null);
                                        }
                            }

                    });





                }
                else
                    {
                        callback(null,{"status":1,"message":" There are no installers found with this ID"});

                    }

            }



    });

}
exports.deleteInstallerFromProject = function(details,callback){
    
    
        var arrayOfInstallers=[];
        
                                // for(var i=0;i<details.installer_id.length;i++){
                                //     arrayOfInstallers.push(ObjectID(details.installer_id[i]));
                                // }
                                _.each(details.installer_id,function(values){
                                    arrayOfInstallers.push(ObjectID(values))
                                })
                                var query={
                                    _id:{$in:arrayOfInstallers},
                                    "organization_Id.organization_Id":new ObjectID(details.organization_Id)
                                }


        mongodb.findByObjects('Users',query,function(findObjerr,installers){
            if(findObjerr){
                callback(err);
            }
            else
                {
                    if(installers.length>0){


                        var array=[];
                        
                                                // for(var i=0;i<details.installer_id.length;i++){
                                                //     arrayOfInstallers.push(ObjectID(details.installer_id[i]));
                                                // }
                                                _.each(installers,function(values){
                                                    array.push(ObjectID(values._id))
                                                })

                        var queryToUpdate={
                            _id:ObjectID(details._id),
                            "installer_id.installer_id":{$in:array}
                        }
                            
                        
                        mongodb.updateManyByObjects(Globals.ProjectCollectionName,queryToUpdate,{$set:{"installer_id.$.installerIsActive":0}},function(err,result)
                        {
                            if (err) callback(err);
                            else
                                {
                                    if(result.result.n>0)
                                    {
                                   
                                            var queryOfInstaller={
                                                installer_id:{$in:arrayOfInstallers},
                                                projectId:new ObjectID(details._id),
                                                "organization_Id":new ObjectID(details.organization_Id)
                                            }
                                            
                                            mongodb.updateManyByObjects(Globals.AssignmentsCollectionName,queryOfInstaller,{$set:{"isActive":0}},function(errInstal,result)
                                            {
                                                if(errInstal) callback(errInstal)
                                                else
                                                {
                                                    if(result)
                                                    {
                                                        callback(null,result);
                                                    }
                                                    else
                                                    {
                                                        callback(null);
                                                    }
                                                }

                                            })
                                        
                                       
                                        }
                                        else
                                            {
                                                callback(null,result);
                                            }
                                }
                        
                        });

                    }
                    else
                        {
                            callback(null,{"status":1,"message":" There are no installers found with this ID"});
                            
                        }
    
                }
       
        });        
    
    
 };