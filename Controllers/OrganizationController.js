var express = require('express')
, router = express.Router()
, bcrypt = require('bcrypt')
, log4js = require('log4js')
, Globals = require('../globals')
, mongodb = require('../db')
, speakeasy = require('speakeasy')
, mailer = require('../mailer')
,ejs = require('ejs')
_ = require('underscore')
, ObjectID = require('mongodb').ObjectID;

const saltRounds = 10;


var organization = require('../Models/Organization');
var installer = require('../Models/Installer');

var secret = speakeasy.generateSecret({length: 20});


exports.addNewOrganization = function(organizationDetails,callback)
{
   var newUser;
   var OTP = generateOTP();

   var queryForOrganization;
    if(organizationDetails.isOrganization==true)
        {
            newUser= new organization(organizationDetails.organization_name,organizationDetails.emailId,organizationDetails.password,organizationDetails.phone_number,organizationDetails.organization_location,organizationDetails.isOrganization,false,organizationDetails.images);
            queryForOrganization ={
                emailId: newUser.emailId,
               // isOrganization:true
                
            };
        }
        else
            {

                newUser= new installer(organizationDetails.organization_Id,organizationDetails.installer_first_name,organizationDetails.installer_last_name,organizationDetails.emailId,organizationDetails.password,organizationDetails.phone_number,organizationDetails.isOrganization,false);
                organizationDetails.password=OTP;
                
                newUser.organization_Id.push({organization_Id:new ObjectID(organizationDetails.organization_Id),isActive:1});

                queryForOrganization ={
                    emailId: newUser.emailId,
                   // isOrganization:false
                };
            }

            
 

mongodb.findByObjects('Users',queryForOrganization,function(errInOrganization,OrganizationExist){

    if(errInOrganization)
                    {
                        callback(errInOrganization);
                        
                    }
    else
        {
            if(OrganizationExist.length>0)
                {
                    if(OrganizationExist[0].isOrganization==true){   
                    callback(null,{"status":1,"message":" Email Id already exist"});
                    }
                    else
                        {
                           delete OrganizationExist[0].password
                            callback(null,{"status":2,"message":" Installer already exist","result":OrganizationExist[0]});
                            
                        }
                    
                }
          else
            {

                bcrypt.hash(organizationDetails.password, saltRounds, function(errInPassword, hash) {
                    // Store hash in your password DB.

                    if(errInPassword) callback(errInPassword);
                    else
                        {

                          
                          var str;
                            if(newUser.isOrganization==true){
                                str = fs.readFileSync(Globals.appRoot+'/public/emailTemplet.ejs', 'utf8');                                
                                newUser.password.push(hash);
                                newUser.OTP = OTP;
                            }else
                            {
                                str =  fs.readFileSync(Globals.appRoot+'/public/installerEmailTemplet.ejs', 'utf8');
                                newUser.password.push(OTP);
                                newUser.OTP = OTP;
                            }
                           

                            mongodb.save('Users',newUser,function(err,OrganizationResults){
                                if (err) {
                                    callback(err);
                                        }
                                  else
                                        {
                                            
                                                var renderedHtml = ejs.render(str, {temp: OTP,emailId:OrganizationResults.emailId}); 
                                            mailer.sendMail(OrganizationResults.emailId,'Welcome To StreetLight','Hi',renderedHtml,function(error,result){
                                                if(error) console.log('unable to send the OTP');
                                                
                                            });
                                            //callback(null,OrganizationResults);
                                            callback(null,{"status":0,"message":" Created successfully","result":OrganizationResults});
                                            
                                            
                                        }
                                  });

                        }
                  });
                
                    
                      
            }
      }

  });

};

exports.OTPConfirmation=function(OrganizationDetails,callback){

    mongodb.findById('Users',OrganizationDetails._id,function(err,result){
        if(err){
            callback(err);
        }
        else
        {
            if(result){

                mongodb.updateById('Users',result._id,{$set:{isAuthenticated:true,loginNumber:1}},function(updateError,result){
                    if(updateError) callback(updateError);
                    else
                        {
                            if(result.result.n==1)
                                {
                                  //  callback(null,OrganizationExist[0]);

                                    callback(null,result);
                                    
                                }
                                else
                                    {
                                        callback(null);
                                    }
                        }

                });
            }
            else
            {
                callback(null,{"status":1,"message":"Unable to find the origanization please check your details"});
            }
        }

    })

}


exports.addCurrentInstallerToThereOrganization = function(OrganizationDetails,callback){

    mongodb.findById('Users',OrganizationDetails._id,function(err,result){
        if(err) callback(err)
            else
            {
                if(result){
                    mongodb.updateById('Users',result._id,{$push:{organization_Id:{organization_Id:new ObjectID(OrganizationDetails.organization_Id),isActive:1}}},function(errUpdate,resultDetails){
                        if(errUpdate) callback(err)
                            else
                                {
                                    if(resultDetails.result.n>0)
                                        {
                                    callback(null,{"status":0,"message":"Added installer successfully"});
                                        }
                                        else
                                            {
                                                callback(null,{"status":1,"message":"Unable to add installer to organization please try again later"});
                                                
                                            }
                                }

                    });


                }
                else
                    {
                        callback(null,{"status":1,"message":"Installer not found please check the installer"});
                        
                    }
            }
    });

};

exports.login = function(OrganizationDetails,callback)
{

    var queryForOrganization ={
        emailId: OrganizationDetails.emailId
    };
    mongodb.findByObjects('Users',queryForOrganization,function(err,OrganizationExist){

        if(err)
            {
                callback(err);
                
            }
            else if(OrganizationExist.length>0)
                {
                    if(OrganizationExist[0].isAuthenticated == false){

                        if(OrganizationExist[0].loginNumber==1){
                            if(OrganizationExist[0].OTP  == OrganizationDetails.password )
                                {
    
                                    if(OrganizationExist[0].isOrganization==true)
                                        {
                                            OrganizationExist[0].isAuthenticated = true;
                                                                
                                            
                                        }
                                    else
                                        {
                                            OrganizationExist[0].isAuthenticated = false;
                                                          
                                            
                                        }
                                    mongodb.updateById('Users',OrganizationExist[0]._id,{$set:{isAuthenticated:true}},function(updateError,result){
                                        if(updateError) callback(updateError);
                                        else
                                            {
                                                if(result.result.n==1)
                                                    {
                                                       // callback(null,OrganizationExist[0]);
                                                        callback(null,{"status":0,"message":" Login successfully","result":OrganizationExist[0]});
                                                        
                                                    }
                                                    else
                                                        {
                                                            callback(null);
                                                        }
                                            }
    
                                    });
                                }
                                else
                                    {
                                        
                                            var lastPasswordIndexIs = OrganizationExist[0].password.length-1;
                                            bcrypt.compare(OrganizationDetails.password,OrganizationExist[0].password[lastPasswordIndexIs], function(err, doesMatch){
                                                if (doesMatch){


                                                    mongodb.updateById('Users',OrganizationExist[0]._id,{$set:{isAuthenticated:true}},function(updateError,result){
                                                        if(updateError) callback(updateError);
                                                        else
                                                            {
                                                                if(result.result.n==1)
                                                                    {
                                                                      //  callback(null,OrganizationExist[0]);
                                                                        callback(null,{"status":0,"message":"Login successfully","result":OrganizationExist[0]});
                                                                        
                                                                    }
                                                                    else
                                                                        {
                                                                            callback(null);
                                                                        }
                                                            }
                    
                                                    });
                                                    
                                                   //log him in
                                                }else{
                                                   //go away
                                                   callback(null);
                                                   
                                                }
                                               });
                                        
                                    }
                        }
                        else
                        {
                            if(OrganizationExist[0].OTP  == OrganizationDetails.password )
                                {
    
                                    var authentication;
                                    var loginnumber;
                                    if(OrganizationExist[0].isOrganization==true)
                                        {
                                            OrganizationExist[0].isAuthenticated = true;
                                            authentication = true
                                            loginnumber=1;
                                                                
                                            
                                        }
                                    else
                                        {
                                            OrganizationExist[0].isAuthenticated = false;
                                            authentication = false
                                            loginnumber=0;
                                                          
                                            
                                        }
                                    mongodb.updateById('Users',OrganizationExist[0]._id,{$set:{isAuthenticated:authentication,loginNumber:loginnumber}},function(updateError,result){
                                        if(updateError) callback(updateError);
                                        else
                                            {
                                                if(result.result.n==1)
                                                    {
                                                      //  callback(null,OrganizationExist[0]);

                                                        callback(null,{"status":0,"message":"Login successfully","result":OrganizationExist[0]});
                                                        
                                                    }
                                                    else
                                                        {
                                                            callback(null);
                                                        }
                                            }
    
                                    });
                                }
                                else
                                    {
                                        callback(null,{"status":1,"message":"Please authenticate your OTP and try again"});
                                    }
                        }
                        
                    }
                    else
                        {
                            var lastPasswordIndexIs = OrganizationExist[0].password.length-1;
                            bcrypt.compare(OrganizationDetails.password,OrganizationExist[0].password[lastPasswordIndexIs], function(err, doesMatch){
                                if (doesMatch){
                                  //  callback(null,OrganizationExist[0]);
                                    callback(null,{"status":0,"message":"Login successfully","result":OrganizationExist[0]});
                                    
                                    
                                   //log him in
                                }else{
                                   //go away
                                   callback(null);
                                   
                                }
                               });
                        }
                  
                
                }
                else
                    {
                        callback(null,{"status":1,"message":"Email ID not exist"});
                        
                    }
   
    });
}





exports.changePassword = function(details,callback){
    
        var collection;

        var queryForOrganization ={
            emailId:details.emailId
        };

    
        mongodb.findByObjects('Users',queryForOrganization,function(findObjerr,OrganizationExist){
if(findObjerr) callback(findObjerr);

else
    {
        if(OrganizationExist.length>0)
            {

                var arr = OrganizationExist[0].password.slice(Math.max(OrganizationExist[0].password.length - 3));

                var calls = [];

             
                arr.forEach(function(password) {
                        calls.push(checkValue(details.password,password));
                })
                
                Promise.all(calls)    
                .then(function(data){ 

                    var filterResult = data.filter((x) => { return x == true; });

                    if(filterResult.length>0){
                        callback(null,{"status":1,"message":"Current password cannot be same as your last three passwords.Please try again with new password"});
                        
                    }
                    else
                        {
                              bcrypt.hash(details.password, saltRounds, function(errPassword, hash) {
                    if(errPassword){
                               callback(null,{"status":1,"message":"Unable to change your password"});
                        }
                        else
                            {
                                details.password = hash;

                                mongodb.updateById('Users',OrganizationExist[0]._id,{$set:{isAuthenticated:true,loginNumber:1},$push:{password:details.password}},function(err,result)
                                {
                                    if (err) callback(err);
                                    else
                                        {
                                            if(result)
                                                {
                                                    //callback(null,result);
							 var queryForOrganization1 ={
                                                        emailId:details.emailId
                                                    };
                                            
                                                
                                                    mongodb.findByObjects('Users',queryForOrganization1,function(findObjerr,OrganizationExist1){
                                                            if(findObjerr) callback(err)
                                                            else
                                                            {
                                                                var resultss={
                                                                    result :result,
                                                                    useDetails:OrganizationExist1
                                                                }
                                                                callback(null,resultss);
                                                            }
                                                    })
                                                }
                                                else
                                                    {
                                                        callback(null);
                                                    }
                                        }
                                
                                });
                            }
                });
                        }
                    
                    
                })
                .catch(function(err){ 
                    callback(err);
                });
               

              
            }
            else
                {
                    callback(null,{"status":1,"message":" Email ID not exist"});
                }
    }




        });
    
                var idOfUser = details.idOfUser
                delete details.isOrganization;
                delete details.idOfUser;
    
    };
    

    function checkValue(password1,password2){
        return new Promise(function(resolve, reject){


            bcrypt.compare(password1,password2, function(err, doesMatch){
                if(err) {
                    console.log("asdfasdfsadf");
                    reject(err);
                }
                else if(doesMatch)
                    {
                        resolve(doesMatch);
                    }
                    else
                        {
                            resolve(doesMatch);
                            
                        }
                });

       
       });
    };

exports.forgotPassword = function(details,callback){
    var queryForOrganization ={
        emailId:details.emailId
    };


    mongodb.findByObjects('Users',queryForOrganization,function(findObjerr,OrganizationExist){
            if(findObjerr) callback(findObjerr);
            else
                {
                    if(OrganizationExist.length>0)
                        {
                                var OTP= generateOTP();

                            if(OrganizationExist[0].isOrganization==true){
                                str = fs.readFileSync(Globals.appRoot+'/public/emailTemplet.ejs', 'utf8');                                
                            }else
                            {
                                str =  fs.readFileSync(Globals.appRoot+'/public/installerEmailTemplet.ejs', 'utf8');
                            }

                            var renderedHtml = ejs.render(str, {temp: OTP,emailId:OrganizationExist[0].emailId}); 
                            mailer.sendMail(OrganizationExist[0].emailId,'Welcome To StreetLight','Hi',renderedHtml,function(error,result){
                                if(error)
                                    {
                                     console.log('unable to send the OTP');
                                     callback(null,{"status":1,"message":" Unable to send the OTP to your email Please check your emailID"});
                                    }

                                else
                                    {
                                        mongodb.updateById('Users',OrganizationExist[0]._id,{$set:{isAuthenticated:false,OTP:OTP}},function(err,result)
                                        {
                                            if (err) callback(err);
                                            else
                                                {
                                                    if(result)
                                                        {
                                                        var res = {"result":result,"OTP":OTP,"isOrganization":OrganizationExist[0].isOrganization,"_id":OrganizationExist[0]._id}
                                                            callback(null,res);  
							//  callback(null,result);
                                                        }
                                                        else
                                                            {
                                                                callback(null);
                                                            }
                                                }
                                        
                                        });
                                    }
                                
                            });

                        }
                        else
                            {
                                callback(null,{"status":1,"message":" Email ID not exist"});
                                
                            }
                }
        });
        
};

// exports.resendOTP=function(details,callback){

//     var str;
    

//     mongodb.findById("Users",ObjectID(details._id),function(err,result){
//         if(err){
//             callback(err);
//         }
//         else
//         {
//             if(result){
            

//                 var OTP= generateOTP();
                
//                                             if(result.isOrganization==true){
//                                                 str = fs.readFileSync('./public/emailTemplet.ejs', 'utf8');                                
//                                             }else
//                                             {
//                                                 str =  fs.readFileSync('./public/installerEmailTemplet.ejs', 'utf8');
//                                             }
                
//                                             var renderedHtml = ejs.render(str, {temp: OTP}); 
//                                             mailer.sendMail(result.emailId,'Welcome To StreetLight','Hi',renderedHtml,function(error,result){
//                                                 if(error)
//                                                     {
//                                                      console.log('unable to send the OTP');
//                                                      callback(null,{"status":1,"message":" Unable to send the OTP to your email Please check your emailID"});
//                                                     }
                
//                                                 else
//                                                     {
//                                                         mongodb.updateById('Users',result._id,{$set:{isAuthenticated:false,OTP:OTP}},function(err,result)
//                                                         {
//                                                             if (err) callback(err);
//                                                             else
//                                                                 {
//                                                                     if(result)
//                                                                         {
//                                                                             callback(null,result);
//                                                                         }
//                                                                         else
//                                                                             {
//                                                                                 callback(null);
//                                                                             }
//                                                                 }
                                                        
//                                                         });
//                                                     }
                                                
//                                             });


//             }
//         }
//     })
                                


// }


exports.getAllInstallersOfOrganization = function(details,callback){

    var queryForAllInstallers ={
        organization_Id:new ObjectID(details.organization_Id)
    };



    mongodb.findByObjectsWithFields('Users',queryForAllInstallers,{installer_first_name:1,installer_last_name:1,emailId:1,phone_number:1,_id:1},function(findObjerr,installers){
        if(findObjerr){
            callback(err);
        }
        else
            {
                if(installers.length>0){
                    
                    callback(null,installers);
                }
                else
                    {
                        callback(null,{"status":1,"message":" There are no installers added in this organization"});
                        
                    }

            }
   
    });        


};

exports.deleteInstaller = function(details,callback){
    
    
        var arrayOfInstallers=[];
        
                                // for(var i=0;i<details.installer_id.length;i++){
                                //     arrayOfInstallers.push(ObjectID(details.installer_id[i]));
                                // }
                                _.each(details.installer_id,function(values){
                                    arrayOfInstallers.push(ObjectID(values))
                                })
                                var query={
                                    _id:{$in:arrayOfInstallers},
                                    "organization_Id.organization_Id":new ObjectID(details.organization_Id)
                                }


        mongodb.findByObjects('Users',query,function(findObjerr,installers){
            if(findObjerr){
                callback(err);
            }
            else
                {
                    if(installers.length>0){



                        
                        mongodb.updateManyByObjects('Users',query,{$set:{"organization_Id.$.isActive":0}},function(err,result)
                        {
                            if (err) callback(err);
                            else
                                {
                                    if(result)
                                        {
                                            var queryOfInstaller={
                                                installer_id:{$in:arrayOfInstallers},
                                                "organization_Id":new ObjectID(details.organization_Id)
                                            }
                                            
                                            mongodb.updateManyByObjects(Globals.AssignmentsCollectionName,queryOfInstaller,{$set:{"isActive":0}},function(errInstal,result)
                                            {
                                                if(errInstal) callback(errInstal)
                                                else
                                                {
                                                    if(result)
                                                    {
                                                        callback(null,result);
                                                    }
                                                    else
                                                    {
                                                        callback(null);
                                                    }
                                                }

                                            })
                                           
                                           
                                              

                                        }
                                        else
                                            {
                                                callback(null);
                                            }
                                }
                        
                        });




                        
                    }
                    else
                        {
                            callback(null,{"status":1,"message":" There are no installers found with this ID"});
                            
                        }
    
                }
       
        });        
    
    
 };

 exports.getAllOrganizationsOfInstallerInvolvedIn=function(details,callback){
 
    mongodb.findById('Users',details.installer_id,function(err,result){
        if(err) callback(err);
        else
            {

                if(result)
                    {
                var arrayOfInstallers=[];
                
                // for(var i=0;i<result.organization_Id.length;i++){
                //     if(result.organization_Id[i].isActive == 0)
                //         {
                //              arrayOfInstallers.push(ObjectID(result.organization_Id[i].organization_Id));
                //         }
                // }

                _.each(result.organization_Id,function(values){
                    if(values.isActive==1){
                        arrayOfInstallers.push(ObjectID(values.organization_Id));
                    }
                })
                var query={
                    _id:{$in:arrayOfInstallers}
                                }
                                      //  mongodb.findByObjectsWithFields('Users',query,{installer_first_name:1,installer_last_name:1,emailId:1},function(errInstaller,organizations){
                                        mongodb.findByObjectsWithFields('Users',query,{organization_name:1,emailId:1,phone_number:1,_id:1},function(errOrg,organizations){
                                            if(err) callback(errOrg);
                                            else
                                                {
                                                    if(organizations.length>0)
                                                        {
                                                            organizations.forEach(function(v){ delete v.password });
                                                            
                                                            callback(null,{"status":0,"message":" Successfully retreived organization details.","result":organizations});
                                                        }
                                                        else
                                                            {
                                                                callback(null,{"status":1,"message":" There are no organizations for you."});
                                                            }
                                                }
                                        })



                    }   else
                    {

                        callback(null,{"status":1,"message":" There are no organizations ."});
                        
                    }
                
            }


    });

};

var projectController = require('../Controllers/ProjectController');
var installationController = require('../Controllers/InstallationController');





exports.getCompleteDetailsOfThisOrganization=function(details,callback){

    
    mongodb.findById("Users",details._id,function(orgErr,organizationResult){
        if(orgErr){
            callback(orgErr);
        }
        else
        {
            if(organizationResult)
            {

                if(organizationResult.isOrganization==true)
                {

                    var qqqq={
                        "organization_Id.organization_Id":organizationResult._id,
                        "organization_Id.isActive":1
                    }
             mongodb.findByObjects("Users",qqqq,function(orgErr1,organizationInstallers){
                        if(orgErr1){
                            callback(orgErr1);
                        }
                        else
                        {
                            if(organizationInstallers)
                            {
                               var arrray=[];
                               var promiseArray=[];

                                _.each(organizationInstallers,function(vv){
                                  promiseArray.push(
                                        new Promise(function(resolve,reject){

                                          
                                            var obj={
                                                "_id":vv._id,
                                                "emailId":vv.emailId,
                                                "installer_first_name":vv.installer_first_name,
                                                "installer_last_name":vv.installer_last_name
                                            }
                                            arrray.push(obj);
                                            
                                            resolve(1)
                                               
                                                
                                                    
                                                    
                                                
                                                
                                                
                                                                                                       
                                                                                                             
					
                                             
                                                                                    
                                                  
                                                   
                                               
        
                                           

                                        })


                                  )    
                                   
                                })

                                Promise.all(promiseArray).then(function(data){


var query={
                    organization_Id:organizationResult._id
                }
                projectController.allProjectsOfAnOrganization(query,function(errProject,projResult){
                    if(errProject){
                        callback(errProject)
                    }
                    else

                    {
                        var allDetailsWithThereProjects=[];
                                                if(projResult.result)
                        {
                            //organizationResult["Projects"]= projResult.result;

                            var installersArray=[];
                            var emptyInstallersArray=[];
                            var arrayOfIds=[];
                            _.each(projResult.result,function(values){

                                if(values.installer_id.length>0)
                                {
                                    installersArray.push(values)
                                    arrayOfIds.push(values._id)
                                }
                                else
                                {
                                    emptyInstallersArray.push(values);
                                }
                            })

                         

                            var query=[
                               {$match:{"_id":{"$in":arrayOfIds}}},
                                {$unwind:"$installer_id"},
                                {"$lookup":{from:'Users',localField:'installer_id.installer_id',foreignField:'_id',as:'installer_id'}},
                                {$unwind:"$installer_id"},
                                 {$group:{_id:"$_id","organization_Id":{$first:"$organization_Id"},"project_name":{$first:"$project_name"},"project_description":{$first:"$project_description"},"time_stamp":{$first:"$time_stamp"},"images":{$first:"$images"},"projectIsActive":{$first:"$projectIsActive"},installer_id:{"$push":"$installer_id"}}},
                                 {$project:{"_id":1,"organization_Id":1,"project_name":1,"project_description":1,"time_stamp":1,"images":1,"projectIsActive":1,"installer_id._id":1,"installer_id.installer_first_name":1,"installer_id.installer_last_name":1,"installer_id.emailId":1}},                                  
                                {$unwind:"$installer_id"},
                                {"$lookup":{from:'Assignments',localField:'installer_id._id',foreignField:'installer_id',as:'installer_id.assignments'}},
                                {
                                    "$redact": {
                                      "$cond": [
                                        {
                                          "$eq": [
                                            {
                                              "$ifNull": [
                                                "$projectId",
                                                "$$ROOT._id"
                                              ]
                                            },
                                            "$$ROOT._id"
                                          ]
                                        },
                                        "$$DESCEND",
                                        "$$PRUNE"
                                      ]
                                    }
                                  },
                                {$unwind:"$installer_id.assignments"},
                                {
                                    "$redact": {
                                      "$cond": [
                                        {
                                          "$eq": [
                                            "$installer_id.assignments.projectId",
                                            "$_id"
                                          ]
                                        },  
                                        "$$KEEP",
                                        "$$PRUNE"
                                      ]
                                    }
                                  },
                                 {$group:{_id:"$_id","organization_Id":{$first:"$organization_Id"},"project_name":{$first:"$project_name"},"project_description":{$first:"$project_description"},"time_stamp":{$first:"$time_stamp"},"images":{$first:"$images"},"projectIsActive":{$first:"$projectIsActive"},installer_id:{"$push":"$installer_id"},"installer_first_name":{$first:"installer_id.installer_first_name"},"installer_last_name":{$first:"installer_id.installer_last_name"} }},
                                 {$project:{"_id":1,"organization_Id":1,"project_name":1,"project_description":1,"time_stamp":1,"images":1,"projectIsActive":1,"installer_id._id":1,"installer_id.installer_first_name":1,"installer_id.installer_last_name":1,"installer_id.emailId":1,"installer_id.assignments._id":1,"installer_id.assignments.projectId":1,"installer_id.assignments.organization_Id":1,"installer_id.assignments.installer_id":1,"installer_id.assignments.isActive":1,}},                                  
                                 {$unwind:"$installer_id"},
                                 {$unwind:"$installer_id.assignments"},
                                 {"$lookup":{from:'Installations',localField:'installer_id.assignments._id',foreignField:'generalInformation.assignment_id',as:'installer_id.assignments.installations'}},
                                 {$group:{_id:"$_id","organization_Id":{$first:"$organization_Id"},"project_name":{$first:"$project_name"},"project_description":{$first:"$project_description"},"time_stamp":{$first:"$time_stamp"},"images":{$first:"$images"},"projectIsActive":{$first:"$projectIsActive"},installer_id:{"$push":"$installer_id"},"installer_first_name":{$first:"installer_id.installer_first_name"},"installer_last_name":{$first:"installer_id.installer_last_name"} }},
                                 {$project:{"_id":1,"organization_Id":1,"project_name":1,"project_description":1,"time_stamp":1,"images":1,"projectIsActive":1,"installer_id._id":1,"installer_id.installer_first_name":1,"installer_id.installer_last_name":1,"installer_id.emailId":1,"installer_id.assignments._id":1,"installer_id.assignments.projectId":1,"installer_id.assignments.organization_Id":1,"installer_id.assignments.installer_id":1,"installer_id.assignments.isActive":1,"installer_id.assignments.installations":1}},                                  
                            
                                 
                        
                            ]

                             mongodb.findAllDetails(Globals.ProjectCollectionName,query,function(errFind,resultProjects){
                                 if(errFind) callback(errFind)
                                 else
                                 {

                                    //callback(null,resultProjects);
                                   
                                    
                        
                                  allDetailsWithThereProjects=resultProjects.concat(emptyInstallersArray);
                                     callback(null,{"status":0,"message":"Details.","result":allDetailsWithThereProjects,"nonAssignedUsers":arrray});
                                     
                                 }
                             })
                            
                        }
                        else
                        {
                            callback(null,{"status":0,"message":"Details.","result":allDetailsWithThereProjects,"nonAssignedUsers":arrray});
                            
                        }
                    }

                })

                                  //  callback(null,arrray);
                                }).catch(function(err){
                                    callback(err)
                                })



                        
                        } 
                        else
                        {
                            callback(null,{"status":0,"message":"Details.","result":allDetailsWithThereProjects,"nonAssignedUsers":arrray});
                            

                        }
                        

                        }
                    })


            }
            else
            {

                
                


                 var query=[
                                {$match:{"_id":new ObjectID(details._id)}},
                                {$unwind:"$organization_Id"},
                                {"$lookup":{from:"Users",localField:'organization_Id.organization_Id',foreignField:'_id',as:'organization_Id'}},
                                {$unwind:"$organization_Id"},
                                {$group:{_id:"$_id",organization_Id:{"$push":"$organization_Id"}}},
                                 {$project:{"organization_Id.organization_name":1,"organization_Id._id":1,"organization_Id.organization_name":1,"organization_Id.emailId":1,"organization_Id.phone_number":1}},
                                 {$unwind:"$organization_Id"},
                                 {"$lookup":{from:Globals.ProjectCollectionName,localField:'organization_Id._id',foreignField:'organization_Id',as:'organization_Id.projects'}},
                                 {$group:{_id:"$_id",organization_Id:{"$push":"$organization_Id"}}},
                                  {$project:{"organization_Id.organization_name":1,"organization_Id._id":1,"organization_Id.organization_name":1,"organization_Id.emailId":1,"organization_Id.phone_number":1,"organization_Id.projects":1}},
                                 
                                
                            ]
                            mongodb.findAllDetails('Users',query,function(errFind,resultProjects){
                                if(errFind) callback(errFind)
                                else
                                {
                                    var organizations = resultProjects[0]
                                    var x;
                                    var y;
                                    var arrayOfProjectIds=[];
                                    _.each(organizations.organization_Id,function(organization_Id,idx){
                                        x=idx;
                                        _.each(organization_Id.projects,function(projects,idy){
                                            y = idy;
                                                _.each(projects.installer_id,function(installer_id,idz){
                                                    var installer_idIs= installer_id.installer_id.toString();

                                                    if(projects.installer_id.length>1){
                                                      
                                                        if(installer_idIs == details._id ){
                                                            console.log("values");
                                                        }
                                                        else{
                                                           // delete organization_Id.installer_id
                                                           delete organization_Id.projects[y].installer_id[idz];
                                                            console.log("organization_Id",+organization_Id);
                                                        }
                                                    }
                                                    else
                                                    {

                                                        if(installer_idIs == details._id ){
                                                            console.log("values");
                                                        }
                                                        else{

                                                            
                                                            delete organization_Id.projects[y];
                                                        }
                                                        
                                                    }

                                                })
                                        })

                                     
                                    })

                                    console.log("sending data")

                                    var ssssd=[];
                                    var deletingNullArray=[];
                                    
                                    _.each(organizations.organization_Id[0].projects,function(valuessss,dx){
                                        if(valuessss!=null&& valuessss!=undefined)
                                        {
                                          //  ssssd.push(valuessss);
                                            _.each(valuessss.installer_id,function(values,dy){
                                                if(values!=null && values!=undefined ){
                                                    deletingNullArray.push(values)
                                                }
                                            })
                                            valuessss.installer_id=[];
                                            Array.prototype.push.apply(valuessss.installer_id,deletingNullArray)
                                             if(deletingNullArray.length>0)
                                            {
                                                ssssd.push(valuessss);
                                                
                                            }
                                        }
                                    

                                    })                                       
                                        
                                            
                                        
                                        
                                    
                                    organizations.organization_Id[0].projects=[];
                                   // organizations.organization_Id[0].projects.push(ssssd);
                                    Array.prototype.push.apply(organizations.organization_Id[0].projects,ssssd)
                                    //callback(null,organizations);
                                    _.each(organizations.organization_Id[0].projects,function(valuess){
                                        arrayOfProjectIds.push(valuess._id);
                                        
                                    })
                                    
                                        var queryMatch=[
                                            {$match:{"projectId":{"$in":arrayOfProjectIds},installer_id:new ObjectID(details._id)}},
                                            {"$lookup":{from:'Installations',localField:'_id',foreignField:'generalInformation.assignment_id',as:'installations'}},
                                            {$group:{_id:"$_id","projectId":{$first:"$projectId"},"organization_Id":{$first:"$organization_Id"},"installer_id":{$first:"$installer_id"},"isActive":{$first:"$isActive"},installations:{$first:"$installations"}}},
                                            {$project:{"_id":1,"projectId":1,"organization_Id":1,"installer_id":1,"isActive":1, "installations":1}},                                  
                                        ];
                                        mongodb.findAllDetails('Assignments',queryMatch,function(errFind,resultAssignments){
                                            if(errFind) callback(errFind)
                                            else
                                            {
                                                console.log("organization_Idsssss",+resultAssignments);
                                                

                                                 var project_id;
                                                 var organization_id;
                                                 
                                                    var promise=[];


                                                promise.push(
                                                    new Promise(function(resolve,reject){


                                                        _.each(organizations.organization_Id,function(organization_Id,idx){
                                                            
                                                              organization_id=organization_Id._id.toString();
								if(organization_Id.projects.length>0)
                                                              {
                                                                _.each(organization_Id.projects,function(projects,idy){
                                                               project_id= projects._id.toString();
                                                	var arrayProj =[];            
						      _.each(projects.installer_id,function(installer_ids,idz){
                                                                                                                        
                                                            
                                                                   if(installer_ids){
                                                            
                                                                     var id = installer_ids.installer_id;
                                                                    var active = installer_ids.installerIsActive;
                                                                 _.each(resultAssignments,function(values){
                                                                                                                               
                                                                        var projectId= values.projectId.toString();
                                                                        var organization_IdAssign = values.organization_Id.toString();
                                                                        var installer_idAssign = values.installer_id.toString()
                                                            
                                                            
                                                                            if(installer_idAssign==details._id && projectId==project_id && organization_id==organization_IdAssign){
                                                                                                                                   // installer_id["installations"] = values;
                                                                                projects.installer_id[idz]={
                                                                                    "installer_id":id,
                                                                                    "installerIsActive":active,
                                                                                    "assignments":values
                                                            
                                                                                 }
											arrayProj.push(projects.installer_id[idz]);
	                                                                                resolve(1)
                                                                                                                             
                                                                             }
                                                                         else
                                                                            {
                                                                                resolve(1)
                                                                                 }
                                                                                                                            
                                                                            })
                                                            
                                                                                                                            
                                                                             }
                                                                        else
                                                                             {
                                                                                 resolve(1)
                                                                             }
                                                            
                                                                     })
										if(arrayProj.length>0)
                                                                            {

                                                                            }
                                                                            else
                                                                            {
                                                                                delete organization_Id.projects[idy];
                                                                            }
                                                                 })
}  
                                                                else
                                                                {
                                                                    resolve(1)
                                                                }
                                                            
                                                            })

                                                    })

                                                    
                                                

                                            )

                                        Promise.all(promise).then(function(data){
					var aaa=[];
                                            _.each(organizations.organization_Id[0].projects,function(dd){
                                                if(dd!=null&&dd!=undefined)
                                                {
                                                    aaa.push(dd)
                                                }
                                            })
                                            organizations.organization_Id[0].projects=[];
                                            
                                            Array.prototype.push.apply(organizations.organization_Id[0].projects,aaa)

                                            callback(null,{"status":0,"message":"Details.","result":organizations});
                                            
                                        }).catch(function(err){

                                        })

                                            }
                                        
                                        })


                                  //  callback(null,{"status":0,"message":"Details.","result":organizations});
                                    
                                }
                            })

            }
            }
            else
            {
                callback(null,{"status":1,"message":" Unable to find any details with this ID,please check the details ."});
            }
        }


    })

}
    


function generateOTP(){
    var token = speakeasy.totp({
        secret: secret.base32,
        encoding: 'base32',
        window: 6
      });
      return token;

};
