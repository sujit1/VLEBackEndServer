var Path = require('path');

function Globals() {
};

//DB configurations
Globals.prototype.MongoPort = 27017;
//Current production server
Globals.prototype.MongoHost = 'localhost';
Globals.prototype.MongoDB = 'ITC';
Globals.prototype.appRoot = Path.resolve(__dirname);
Globals.prototype.graceTime = 30000; // in milliseconds used for hub on off status schedule time

Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 100,
            "category": "relative-logger"
        }
    ]
};


/////////////////////////////////////////////////MongoDB Collection Names///////////////////////////////////////
//Globals.prototype.InstallationCollectionName='Installations';



Globals.prototype.OrganizationCollectionName = 'Organizations';
Globals.prototype.ProjectCollectionName ='Projects';
Globals.prototype.InstallerCollectionName = 'Installers';
Globals.prototype.InstallationCollectionName = 'Installations';
Globals.prototype.S3BucketName = 'vlestreetlightbucket';
Globals.prototype.AssignmentsCollectionName = 'Assignments';

Globals.prototype.categorySolarStreetLight ='59f2d7e8bb2fef54e6981793';
Globals.prototype.categorySolarHomeLight='59f2d7e8bb2fef54e6981794';
Globals.prototype.categorySolarInverterSystem='59f2d7e8bb2fef54e6981795';
Globals.prototype.categorySolarLantern='59f2d7e8bb2fef54e6981796';

Globals.prototype.accessKeyId="AKIAI5LF3X76Q5BZNJGA";
Globals.prototype.secretAccessKey="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";
Globals.prototype.region="us-east-1";

module.exports = new Globals();
