var MongoClient = require('mongodb').MongoClient
    , ObjectID = require('mongodb').ObjectID;

    var async = require('async');
    fs = require('fs');
    
    
  var state = {
  db: null,
};


var log4js = require('log4js');
var logger = log4js.getLogger('relative-logger');

var mongoose = require("mongoose");
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
global.conn = mongoose.createConnection('mongodb://localhost:27017/sampletest1');
conn.once('open', function(){
});






 exports.connect = function(host, port, database, callback) {
  if (state.db) return callback();

  MongoClient.connect('mongodb://'+ host +':'+ port +'/'+database, function(err, db) {
    if (err) return callback(err);
    state.db = db;
    exports.dbDetails = state.db;

        global.gfs = Grid(db);

    callback();
  });
};

exports.close = function(callback) {
  if (state.db) {
    state.db.close(function(err, result) {
      state.db = null;
      state.mode = null;
      callback(err);
    });
  }
};








exports.getCollection = function(_collectionName, callback) {
  if(!state.db) return callback("No Db.");

  state.db.collection(_collectionName, function(err, _collection){
    callback(null, _collection);
  });
};

   //insert one object
// ==============================================

   exports.save = function(collection,bodyObject,callback){
   	this.getCollection(collection,function(err,_collection){
   		if (err) callback(err);
   		else{
_collection.insert(bodyObject,function(){
	callback(null,bodyObject);
});
   		}
   	});
   };


   //insert many objects in to db
// ==============================================
   exports.saveMany = function(collection,bodyObject,callback){
     this.getCollection(collection,function(err,_collection){
      if (err) callback(err);
      else{
           _collection.insertMany(bodyObject,function(){
                 callback(null,bodyObject);
                });
         }
      });

   };


   //Insert image to mongoDB




exports.insertImage = function(collection,req,callback){
 this.getCollection(collection,function(err,_collection){
if(err) callback(err);
else
  {
    var calls = [];
    var mongodbs = require('mongodb');
    
    req.forEach(function (files){
    
    calls.push(function(callback){
    
     var bucket = new mongodbs.GridFSBucket(state.db, {
                chunkSizeBytes: 1024,
                contentType:files.mimetype,
                bucketName: 'usersImage'
            });
            
            fs.createReadStream(files.image.path).pipe(
                bucket.openUploadStream(files.image.originalname)).on('error', function(error) {
               return callback(error);
               // console.log('Error:-', error);
            }).on('finish', function(savedData) {
                var parsedResponse = JSON.parse(JSON.stringify(savedData));
                console.log('parsedResponse');
    
                 callback(null,parsedResponse._id);

                 fs.unlink(files.image.path, function() {
                  if (err) throw err;
                  logger.info('File uploaded to: ' + files.image.path + ' - ' + files.image.size + ' bytes');
              });
    
            });
    });
    
    })
    

    async.parallel(calls, function(err, result) {
      /* this code will run after all calls finished the job or
         when any of the calls passes an error */
      if (err)
          return console.log(err);
      logger.info(result);
      callback(null,result);
 
      });

  }

 });

};


   //find  by user ID
// ==============================================

   exports.findById = function(collection, id, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
      _collection.findOne({_id: new ObjectID(id)}, function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });
};

exports.findAllDetails = function(collection, query, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
      
      _collection.aggregate(query,function(err,result){
        if(err) callback(err)
        else
        {
          callback(null,result);
        }
      })
    }
  });
};
   //find by object ID
// ==============================================
exports.findByObjects = function(collection, query, callback) {
    this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {

        //console.log("my qurry to check is "+JSON.stringify(query));
      _collection.find(query).toArray(function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });
};


exports.findNestedObjects=function(collection, query, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
      _collection.aggregate([query],function(err,result){
        if( error ) callback(error)
        else callback(null, result)

      })

    }


  });
}

exports.findByObjectsWithFields = function(collection, query,requiredFields, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {

        //console.log("my qurry to check is "+JSON.stringify(query));
      _collection.find(query,requiredFields).toArray(function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });

}

   //find all
// ==============================================
   exports.findAll = function(collection,callback){
this.getCollection(collection,function(err,_collection){
if (err)  callback(err);
else
   {
   _collection.find().toArray(function(err,results){
   	if (err)  callback(err);
   	else
   	{
callback(null,results);
   	}
   });
   }
});

   };


   //delete one
// ==============================================
exports.deleteOneByID = function(collection, _id, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
      _collection.deleteOne({_id: new ObjectID(_id)}, function(err, result) {
        callback(null);
      });
    }
  });
};

//delete many
// ==============================================
exports.deleteManyByObject = function(collection, providerObjects, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
      _collection.deleteMany(providerObjects, function() {
        callback(null);
      });
    }
  });
};

   //update by  objectID
// ==============================================
exports.updateById = function(collection,_id,setObjects, callback) {
  this.getCollection(collection, function(error, _collection) {
  if( error ) callback(error)
  else {
      //console.log("my qurry to check is "+JSON.stringify(query));
    _collection.update({'_id':_id},setObjects,function(error, result) {
      if( error ) callback(error)
      else callback(null, result)
    });
  }
});
};

exports.updateManyByObjects= function(collection,conditionalObjects,setObjects,callback){
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
        //console.log("my qurry to check is "+JSON.stringify(query));
      _collection.updateMany(conditionalObjects,setObjects,function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });
};